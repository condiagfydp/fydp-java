package com.example.frameworkapp;

import java.util.Arrays;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.jjoe64.graphview.GraphViewSeries;

public class GraphActivity extends Activity implements OnTaskCompleteListener{

	private AsyncTaskManager taskManager;
	private final String DATABASE_TABLE_NAME = "graph1";
	GraphViewSeries graph1Series;
	private XYPlot graph1Plot;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_graph);
		taskManager = new AsyncTaskManager(this, this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.graph, menu);
		return true;
	}
	
	/** Called when the user clicks the Get button */
    public void getData(View view) {
        // Do something in response to button
    	try {
    		taskManager.setupTask(new AuthenticationTask(getResources(), "select"+DATABASE_TABLE_NAME, null, null), true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
    /*
     * This function reads the response from the server and parses the values.
     */
	public void onTaskComplete(TaskBase<?,?> task) {
		if(task.getClass() == AuthenticationTask.class){
			Object o = null;
			try {
				o = task.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.v("DATABASE", "Received: " + (String) task.mResult);
			if (o != null){
				String result = (String) task.mResult;
				
				int i = 0;
				//first time, determine how many data values are present
				while(result.split(" ")[i].length() < 4){
					i++;
				}
				Log.v("DATABASE", "Number of Data Values: " + i);
				String[] resultArray = new String[i];
				i = 0;
				//second time, store data values in an array
				while(result.split(" ")[i].length() < 4){
					resultArray[i] = result.split(" ")[i];
					i++;
				}
				int[][] graph1Table = new int[resultArray.length/3][2];
				int counter = 1;
				//put array values into a table
				Number[] xArray = new Number[resultArray.length/3];
				Number[] yArray = new Number[resultArray.length/3];
				for(i = 0; i < resultArray.length/3; i++) {
					for(int j = 0; j < 2; j++){
						graph1Table[i][j] = Integer.parseInt(resultArray[counter]);
						counter++;
					}
					xArray[i] = graph1Table[i][0];
					yArray[i] = graph1Table[i][1];
					counter++;
					Log.v("DATABASE", "Row " + i + ": " + graph1Table[i][0] + ", " + graph1Table[i][1]);
				}
				//graph1Table = new int[][]{{0,0},{1,0},{1,1},{0,1}};
//				GraphViewData[] graph1Data = new GraphViewData[resultArray.length/3];
				graph1Plot = (XYPlot) findViewById(R.id.graph1);
				graph1Plot.clear();
				XYSeries graph1Series = new SimpleXYSeries(Arrays.asList(xArray), Arrays.asList(yArray), "Graph1");
				LineAndPointFormatter series1Format = new LineAndPointFormatter(
		                Color.rgb(0, 200, 0),                   // line color
		                Color.rgb(0, 100, 0),                   // point color
		                null,                                   // fill color (none)
		                new PointLabelFormatter(Color.WHITE)); 
				graph1Plot.addSeries(graph1Series, series1Format);
				graph1Plot.setTicksPerRangeLabel(3);
				graph1Plot.getGraphWidget().setDomainLabelOrientation(-45);
				graph1Plot.redraw();
//				for(i = 0; i < resultArray.length/3; i++) {
//					graph1Data[i] = new GraphViewData(graph1Table[i][0], graph1Table[i][1]);
//				}
//				graph1Series = new GraphViewSeries(graph1Data);
//				GraphView graphView;
//				graphView = new LineGraphView(this // context
//						, "GraphViewDemo" // heading
//				);
//				graphView.addSeries(graph1Series); // data
//				graphView.getGraphViewStyle().setHorizontalLabelsColor(Color.BLACK);
//				graphView.getGraphViewStyle().setVerticalLabelsColor(Color.BLACK);
//				//graphView.setHorizontalLabels(new String[] {"-20", "-15", "-10", "-5", "0", "5", "10", "15", "20"});
//				//graphView.setVerticalLabels(new String[] {"-20", "-15", "-10", "-5", "0", "5", "10", "15", "20"});
//				LinearLayout layout = (LinearLayout) findViewById(R.id.graph1);
//				layout.addView(graphView);
				
				
			}
			else {
			}
		}
	}

}
