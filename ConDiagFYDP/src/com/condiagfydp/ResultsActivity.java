package com.condiagfydp;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.condiagfydp.database.AsyncTaskManager;
import com.condiagfydp.database.CreateTestDataTask;
import com.condiagfydp.database.OnTaskCompleteListener;
import com.condiagfydp.database.PopulateTestSessionTask;
import com.condiagfydp.database.TaskBase;

public class ResultsActivity extends Activity implements OnTaskCompleteListener{
	private AsyncTaskManager taskManager;

	private ArrayList<String[]> listOfTestData;
	private ArrayList<String> listOfTestIDs;
	private int listCounter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results);
		
		taskManager = new AsyncTaskManager(this, this);
		
		listOfTestData = TestSession.writeTestData(TestSession.getCurrentSession());
		try {
			listCounter = getIntent().getExtras().getInt("listCounter");
		} catch (NullPointerException npe) {
			//if there are no extras, initialize to zero
			listCounter = 0;
		}
		//query DB to create new Test Data entry
		if(listCounter == 14) {
			try {
	    		Log.v("DATABASE", "[ResultsActivity] Sending sessionID: " + TestSession.getCurrentSession().getSessionID() + " testIDs: " + TestSession.getCurrentSession().getTestIDs().toString());
	    		taskManager.setupTask(new PopulateTestSessionTask(getResources(), TestSession.getCurrentSession().getSessionID(), TestSession.getCurrentSession().getTestIDs()), true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (listCounter < 14){
			try {
	    		Log.v("DATABASE", "[ResultsActivity] Sending sessionID: " + TestSession.getCurrentSession().getSessionID() 
	    				+ " testType: " +  listCounter+1 + " test data: " + Arrays.toString(listOfTestData.get(listCounter)));
	    		taskManager.setupTask(new CreateTestDataTask(getResources(), listOfTestData.get(listCounter)), true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.results, menu);
		return true;
	}

	public void returnToProfile(View view) {
		Intent intent = new Intent(this, PatientProfileActivity.class);
		startActivity(intent);
	}

	@Override
	public void onTaskComplete(TaskBase<?, ?> task) {
		if(task.getClass() == CreateTestDataTask.class){
			Object o = null;
			try {
				o = task.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.v("DATABASE", "[ResultsActivity] Received: " + (String) task.mResult);
			if (o != null){
				String result = (String) task.mResult;
				String testID = result.split(" ")[0];
				TestSession.getCurrentSession().getTestIDs().add(listCounter, testID);
				if(listCounter < 15) {
					Bundle b = new Bundle();
					b.putInt("listCounter", listCounter+1);
					Intent intent = getIntent();
					intent.putExtras(b);
				    finish();
				    startActivity(intent);
				}
			}
			else {
			}
		}
	}
}
