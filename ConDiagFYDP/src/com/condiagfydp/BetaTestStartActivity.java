package com.condiagfydp;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BetaTestStartActivity extends Activity {
	
	EditText nameTextField;
	String patientName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_beta_test_start);
		// Show the Up button in the action bar.
		setupActionBar();
		nameTextField = (EditText) findViewById(R.id.nameTextField);
		try {
			String previousPatientName = getIntent().getExtras().getString("patientName");
			Log.v("NAV", "previousPatientName: " + previousPatientName);
			nameTextField.setText(previousPatientName);
		} catch (NullPointerException npe) {
			Log.v("NAV", "no previous patient name");
		}
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		if (btAdapter == null){
			//do something here??
		}
		TextView testDescriptionText = (TextView)findViewById(R.id.testDescription);
		testDescriptionText.setText("The current test is: " + TestCommand.getListOfCommands().get(0));
	}
	
	public void enableBlu() {
		Intent discoveryIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
		discoveryIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
		startActivityForResult(discoveryIntent, 1);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.v("FILE", "I have gotten to the onActivityResultSection" + requestCode + " " + resultCode);
		if(resultCode == 300 && requestCode == 1) {
			Log.v("FILE", "Time to delete the file.");
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test_start, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void sendFile(View view) {
		TestCommand newCommand = new TestCommand(TestCommand.getListOfCommands().get(0));
		newCommand.createFile();
		File dataFile = new File(new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_COMMAND_FOLDERNAME), TestCommand.TEST_DATA_FILENAME);
		Log.v("FILE", "Date File length: " + dataFile.length());
		if(dataFile.exists()) {
			Log.v("FILE", "The Data file exists and will be deleted before writing next command.");
			TestCommand.deleteCommand(dataFile);
		}
		Toast.makeText(this, "Command file has been saved as: " + TestCommand.TEST_COMMAND_FILENAME, Toast.LENGTH_SHORT).show();
	}
	public void testResult(View view) {
		patientName = nameTextField.getText().toString();
		int patientSession = 0;
		File dataFile = new File(new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_COMMAND_FOLDERNAME), TestCommand.TEST_DATA_FILENAME + TestCommand.getListOfCommands().get(0).substring(1) + TestCommand.TEST_DATA_FILETYPE);
		Log.v("FILE", "Date File length: " + dataFile.length());
		if(dataFile.exists()) {
			Log.v("FILE", "The Data file exists.");
			File savedDataFile;
			do {
				patientSession++;
				savedDataFile = new File(new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_DATA_FOLDERNAME), 
					TestCommand.SAVED_DATA_FILENAME + TestCommand.getListOfCommands().get(0) + patientName + "_" + patientSession + TestCommand.SAVED_DATA_FILETYPE);
			} while(savedDataFile.exists());
			TestCommand.copy(dataFile, savedDataFile);
			TestCommand.deleteCommand(dataFile);
			Toast.makeText(this, "Test Data: " + TestCommand.getListOfCommands().get(0) + " saved under /TestData", Toast.LENGTH_SHORT).show();
		} else {
			Log.v("FILE", "File: " + dataFile.getName() + " does not exist.");
			Toast.makeText(this, "Test Data has not been received yet.", Toast.LENGTH_SHORT).show();
		}
//		Intent intent = new Intent(this, Test1ResultActivity.class);
//		startActivity(intent);
	}
	
	public void nextTest(View view) {
		//remove the previous command from the stack to the completed stack
		ArrayList<String> listOfCommands = TestCommand.getListOfCommands();
		if(listOfCommands.get(0) != TestCommand.LAST_TEST_CODE) {
			TestCommand.getListOfCompletedCommands().add(0, listOfCommands.get(0));
			listOfCommands.remove(0);
		} else {
			Toast.makeText(this, "This is the last test.", Toast.LENGTH_SHORT).show();
		}
		Log.v("TESTPROCEDURE", "The updated list of commands: " + listOfCommands.toString());
		Log.v("TESTPROCEDURE", "The updated list of completed commands: " + TestCommand.getListOfCompletedCommands().toString());
		TestCommand.setListOfCommands(listOfCommands);
		patientName = nameTextField.getText().toString();
		Bundle b = new Bundle();
		b.putString("patientName", patientName);
		Intent intent = getIntent();
		intent.putExtras(b);
	    finish();
	    startActivity(intent);
	}
	
	public void previousTest(View view) {
		//add the previous command from the completed stack to the command stack
		ArrayList<String> listOfCommands = TestCommand.getListOfCommands();
		if(listOfCommands.get(0) != TestCommand.FIRST_TEST_CODE) {
			listOfCommands.add(0, TestCommand.getListOfCompletedCommands().get(0));
			TestCommand.getListOfCompletedCommands().remove(0);
		} else {
			Toast.makeText(this, "This is the first test.", Toast.LENGTH_SHORT).show();
		}
		Log.v("TESTPROCEDURE", "The updated list of commands: " + listOfCommands.toString());
		Log.v("TESTPROCEDURE", "The updated list of completed commands: " + TestCommand.getListOfCompletedCommands().toString());
		TestCommand.setListOfCommands(listOfCommands);
		patientName = nameTextField.getText().toString();
		Bundle b = new Bundle();
		b.putString("patientName", patientName);
		Intent intent = getIntent();
		intent.putExtras(b);
	    finish();
	    startActivity(intent);
	}
}
