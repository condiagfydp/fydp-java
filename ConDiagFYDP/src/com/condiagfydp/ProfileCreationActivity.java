package com.condiagfydp;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.condiagfydp.database.AsyncTaskManager;
import com.condiagfydp.database.CreateProfileTask;
import com.condiagfydp.database.OnTaskCompleteListener;
import com.condiagfydp.database.TaskBase;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class ProfileCreationActivity extends Activity implements OnItemSelectedListener, OnTaskCompleteListener {

	//Patient profile information
	String name;
	int age;
	int height;
	int weight;
	String sport;
	boolean isMale;
	boolean isMetric;
	String sGender;
	String sUnits;
	
	//Form fields
	EditText nameField;
	EditText ageField;
	EditText heightField;
	EditText weightField;
	Spinner heightIMSpinner;
	Spinner weightIMSpinner;
	Spinner sportsSpinner;
	Spinner genderSpinner;

	private AsyncTaskManager taskManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_profile_creation);
		setupActionBar();
		
		taskManager = new AsyncTaskManager(this, this);
		
		//setup text fields
		nameField = (EditText)findViewById(R.id.name);
		ageField = (EditText)findViewById(R.id.age);
		heightField = (EditText)findViewById(R.id.height);
		weightField = (EditText)findViewById(R.id.weight);
		
		//setup spinners
		heightIMSpinner = (Spinner) findViewById(R.id.heightIM_spinner);
		heightIMSpinner.setOnItemSelectedListener(this);
		ArrayAdapter<CharSequence> heightIMAdapter = ArrayAdapter.createFromResource(this, R.array.heightIM_array, android.R.layout.simple_spinner_item);
		heightIMAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		heightIMSpinner.setAdapter(heightIMAdapter);
		
		weightIMSpinner = (Spinner) findViewById(R.id.weightIM_spinner);
		weightIMSpinner.setOnItemSelectedListener(this);
		ArrayAdapter<CharSequence> weightIMAdapter = ArrayAdapter.createFromResource(this, R.array.weightIM_array, android.R.layout.simple_spinner_item);
		weightIMAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		weightIMSpinner.setAdapter(weightIMAdapter);
		
		sportsSpinner = (Spinner) findViewById(R.id.sports_spinner);
		ArrayAdapter<CharSequence> sportsAdapter = ArrayAdapter.createFromResource(this, R.array.sports_array, android.R.layout.simple_spinner_item);
		sportsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sportsSpinner.setAdapter(sportsAdapter);
		
		genderSpinner = (Spinner) findViewById(R.id.gender_spinner);
		ArrayAdapter<CharSequence> genderAdapter = ArrayAdapter.createFromResource(this, R.array.gender_array, android.R.layout.simple_spinner_item);
		genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		genderSpinner.setAdapter(genderAdapter);

	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			// TODO: If Settings has multiple levels, Up should navigate up
			// that hierarchy.
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.profile_creation, menu);
		return true;
	}

	/*
	 * Creates the profile based on user entry.
	 */
	public void createProfile(View view) {
		ArrayList<String> formErrorList = new ArrayList<String>();
		name = nameField.getText().toString();
		if(name.equals("")) {
			formErrorList.add("Name");
		}
		try {
			age = Integer.parseInt(ageField.getText().toString());
			if(age <= 0) {
				formErrorList.add("Age");
			}
		} catch (NumberFormatException nfe) {
			formErrorList.add("Age");
		}
		//if metric then store the cm height, if imperial then store the feet height as decimal
		if(isMetric) {
			try {
				height = Integer.parseInt(heightField.getText().toString());
				if(height <= 0) {
					formErrorList.add("Height");
				}
			} catch (NumberFormatException nfe) {
				formErrorList.add("Height");
			}
		} else {
			try {
			String tempHeight = heightField.getText().toString();
			Log.v("MATH", "Imperial Height Calculation - Imperial Height: " + tempHeight);
			Log.v("MATH", "Imperial Height Calculation - Height Feet: " + tempHeight.substring(0, tempHeight.indexOf("\'")));
			Log.v("MATH", "Imperial Height Calculation - Height Inches: " + tempHeight.substring(tempHeight.indexOf("\'")+1, tempHeight.indexOf("\"")));
			double dHeight = Double.parseDouble(tempHeight.substring(0, tempHeight.indexOf("\'"))) 
					+ Double.parseDouble(tempHeight.substring(tempHeight.indexOf("\'")+1, tempHeight.indexOf("\"")))/12;
			height = (int)Math.round(dHeight * 30.48);
			} catch (Exception e) {
				formErrorList.add("Height");
			}
		}
		try {
			weight = Integer.parseInt(weightField.getText().toString());
			if(!isMetric) {
				weight = (int)Math.round((double)weight*0.453592);
			}
			if(weight <= 0) {
				formErrorList.add("Weight");
			}
		} catch (NumberFormatException nfe) {
			formErrorList.add("Weight");
		}
		if(genderSpinner.getSelectedItemPosition() == 0) {
			isMale = true;
			sGender = "m";
		} else {
			isMale = false;
			sGender = "f";
		}
		sport = sportsSpinner.getSelectedItem().toString();
		
		//check if form is valid before submission
		if(formErrorList.isEmpty()) {
			PatientProfile.createPatientProfile(name, age, height, weight, sport, isMale, isMetric);
			try {
	    		Log.v("DATABASE", "Sending Patient name: " + name);
	    		taskManager.setupTask(new CreateProfileTask(getResources(), Integer.toString(age), Integer.toString(height), 
	    				Integer.toString(weight), sport, sGender, sUnits), true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			String error = "";
			for(String formError : formErrorList) {
				error = error + "\n" + formError;
			}
			Toast.makeText(getApplicationContext(), "The following fields are blank/invalid: \n" + error, Toast.LENGTH_LONG).show();
		}
	}
	
	/*
	 * This handles spinner item selection behaviour
	 */
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		if(parent.getItemAtPosition(pos).toString().equals("cm") || parent.getItemAtPosition(pos).toString().equals("kg")) {
			heightIMSpinner.setSelection(0);
			weightIMSpinner.setSelection(0);
			isMetric = true;
			sUnits = "m";
		}
		if(parent.getItemAtPosition(pos).toString().equals("feet\'inch\"") || parent.getItemAtPosition(pos).toString().equals("lbs")) {
			heightIMSpinner.setSelection(1);
			weightIMSpinner.setSelection(1);
			isMetric = false;
			sUnits = "i";
		}
		//Toast.makeText(getApplicationContext(), item, Toast.LENGTH_LONG).show();
	}
	
	/*
	 * This handles non-selection spinner behaviour
	 */
	public void onNothingSelected(AdapterView<?> parent) {
		
	}
	
	@Override
	public void onTaskComplete(TaskBase<?, ?> task) {
		if(task.getClass() == CreateProfileTask.class){
			Object o = null;
			try {
				o = task.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.v("DATABASE", "Received: " + (String) task.mResult);
			if (o != null){
				String result = (String) task.mResult;
				String profileID = result.split(" ")[0].trim();
				Log.v("DATA", name + "'s profile ID is: " + profileID);
				PatientProfile.setPatientID(profileID);
				PatientList.readPatientList();
				PatientList.addNewPatient(name, profileID);
				if(PatientList.writePatientList() == 0) {
					Toast.makeText(getApplicationContext(), name + "'s Profile has been created successfully!", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(this, PatientProfileActivity.class);
					startActivity(intent);
				} else {
					Toast.makeText(getApplicationContext(), "A problem occurred while creating the profile, please try again.", Toast.LENGTH_SHORT).show();
				}
			}
			else {
			}
		}
	}
}
