package com.condiagfydp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.util.Log;

import com.condiagfydp.testTypes.Test1;
import com.condiagfydp.testTypes.Test2;
import com.condiagfydp.testTypes.Test3;
import com.condiagfydp.testTypes.Test4;
import com.condiagfydp.testTypes.Test5;

public class TestProcedure extends Activity {
	private static final String[] TEST_FLOW_CODE_NORMAL = new String[]{"01", "02", "03", "01", "04", "01", "05", "01","02"};
	private static final String CALIBRATION_CODE = "10";
	
	public static boolean isDemo = true;
	private static final String TEST_FLOW_CODE_DEMO = "07";
	private static int testPhase;
	private static final int LAST_TEST_PHASE = 28;
	private static final int LAST_DEMO_TEST_PHASE = 4;
	private static int testNumber;
	private static boolean isCalibrated = false;
	
	//Test Instructions
	private static final String TEST_CALIBRATION_TITLE = "Calibration";
	private static final String TEST_PRE_CALIBRATION_INSTRUCTION = "When ready, press continue and wait for indicator to signal calibration completion.";
	private static final String TEST_POST_CALIBRATION_INSTRUCTION = 
			"When calibration complete:" +
			"\n- Request patient to stand in the middle of the Wii Balance Board, facing forward, feet together in the same position for the duration of the test" +
			"\n- Press continue to proceed to next test";
	private static final String TEST_TEST1_TITLE = "Static Balance Test";
	private static final String TEST_PRE_TEST1_INSTRUCTION = 
			"- Instruct patient to stand still, eyes open, hands at their sides, for 45 seconds until the device indicates end of test" +
			"\n- Instruct patient to perform the same procedure, with eyes closed, for 45 seconds" +
			"\n- Notify the patient when test has ended";
	private static final String TEST_INPROGRESS_INSTRUCTION = "Test in progress, press continue when test has ended.";
	private static final String TEST_RESULT_INSTRUCTION = "Test completed, press continue to proceed to next test.";
	private static final String TEST_TEST2_TITLE = "Extreme Balance Test";
	private static final String TEST_PRE_TEST2_INSTRUCTION = 
			"- Instruct patient, with their hands by their sides and their feet together, to lean forward as far as they can without falling" +
			"\n- After observing end of test notification, instruct patient to repeat the procedure but lean towards the side of their dominant foot";
	private static final String TEST_TEST3_TITLE = "Feedback Balance Test";
	private static final String TEST_PRE_TEST3_INSTRUCTION = 
			"- This feedback test measures Anterior-Posterior balance (Forward and Backward)." +
			"\n- Instruct patient to maintain the LEDs in the centre position until test is completed." +
			"\n- The LEDs will spread outwards when the patient's centre of pressure is not in the centre of the board.";
	private static final String TEST_TEST4_TITLE = "Gait Initiation Test";
	private static final String TEST_PRE_TEST4_INSTRUCTION = 
			"- Instruct patient to take a step forward, with their dominant foot first, off of the Wii Balance Board." +
			"\n- Both feet should end up off of the Wii Balance Board." +
			"\n- When the LEDs signify the end of the measurement, return patient to the previous standing position on the Wii Balance Board." +
			"\n- Repeat steps 4 more times.";
	private static final String TEST_TEST5_TITLE = "Oscillating Balance Test";
	private static final String TEST_PRE_TEST5_INSTRUCTION = 
			"- Instruct patient, with their hands by their sides and their feet together, to stand still, and follow the oscillating LEDs with their eyes only, until the LEDs indicate that the test has finished." +
			"\n- Instruct patient, still with their hands by their sides, and their feet together, to shift their weight/centre of pressure according to the frequecy of the oscillating LEDs, until the LEDs indicate that the test has finished. ";
	private static final String TEST_ERROR_INSTRUCTION = "Error.";
	
	/*
	 * returns the number of the current test in sequence (1 higher than the index)
	 */
	public static int getTestNumber() {
		if(isDemo) {
			return 4;
		}
		return testNumber;
	}
	
	public static int getTestPhase() {
		return testPhase;
	}
	public static boolean isLastPhase() {
		if(isDemo){
			return (testPhase==LAST_DEMO_TEST_PHASE);
		} else {
			return (testPhase==LAST_TEST_PHASE);
		}
	}
	
	public static boolean checkForData() {
		switch(testPhase) {
		case 3:
		case 6:
		case 9:
		case 12:
		case 15:
		case 18:
		case 21:
		case 24:
		case 27:
			return true;
		}
		return false;
	}
	
	public static void resetTest() {
		testPhase = 0;
		testNumber = 0;
	}
	public static void resetDemo() {
		testPhase = 2;
		testNumber = 1;
		TestSession.setArrayOfDemoGaitValues(null);
	}
	public static void nextPhase() {
		testPhase++;
		if((testPhase-2)%3 == 0) {
			testNumber++;
		}
		Log.v("TESTPROCEDURE", "[TestProcedure] testPhase: " + testPhase + " testNumber: " + testNumber);
	}
	
	/*
	 * returns the appropriate description for the current stage of the test
	 */
	public static String getTestPhaseTitle() {
		if(isDemo) {
			switch(testNumber) {
			case 0:
				return TEST_CALIBRATION_TITLE;
			default:
				return TEST_TEST4_TITLE;
			}
		} else {
			switch(testNumber) {
			case 0:
				return TEST_CALIBRATION_TITLE;
			case 1:
				return TEST_TEST1_TITLE;
			case 2:
				return TEST_TEST2_TITLE;
			case 3:
				return TEST_TEST3_TITLE;
			case 4:
				return TEST_TEST1_TITLE;
			case 5:
				return TEST_TEST4_TITLE;
			case 6:
				return TEST_TEST1_TITLE;
			case 7:
				return TEST_TEST5_TITLE;
			case 8:
				return TEST_TEST1_TITLE;
			case 9:
				return TEST_TEST2_TITLE;
			default:
				return TEST_ERROR_INSTRUCTION;
			}
		}
	}
	
	/*
	 * returns the appropriate description for the current stage of the test
	 */
	public static String getTestPhaseDescription() {
		if(isDemo) {
			switch(testPhase) {
			case 0:
				return TEST_PRE_CALIBRATION_INSTRUCTION;
			case 1:
				return TEST_POST_CALIBRATION_INSTRUCTION;
			case 2:
				return TEST_PRE_TEST4_INSTRUCTION;
			case 3:
				isCalibrated = true;
				return TEST_INPROGRESS_INSTRUCTION;
			default:
				return TEST_ERROR_INSTRUCTION;
			}
		} else {
			switch(testPhase) {
			case 0:
				return TEST_PRE_CALIBRATION_INSTRUCTION;
			case 1:
				return TEST_POST_CALIBRATION_INSTRUCTION;
			case 2:
				return TEST_PRE_TEST1_INSTRUCTION;
			case 3:
				return TEST_INPROGRESS_INSTRUCTION;
			case 4:
				isCalibrated = true;
				return TEST_RESULT_INSTRUCTION;
			case 5:
				return TEST_PRE_TEST2_INSTRUCTION;
			case 6:
				return TEST_INPROGRESS_INSTRUCTION;
			case 7:
				return TEST_RESULT_INSTRUCTION;
			case 8:
				return TEST_PRE_TEST3_INSTRUCTION;
			case 9:
				return TEST_INPROGRESS_INSTRUCTION;
			case 10:
				return TEST_RESULT_INSTRUCTION;
			case 11:
				return TEST_PRE_TEST1_INSTRUCTION;
			case 12:
				return TEST_INPROGRESS_INSTRUCTION;
			case 13:
				return TEST_RESULT_INSTRUCTION;
			case 14:
				return TEST_PRE_TEST4_INSTRUCTION;
			case 15:
				return TEST_INPROGRESS_INSTRUCTION;
			case 16:
				return TEST_RESULT_INSTRUCTION;
			case 17:
				return TEST_PRE_TEST1_INSTRUCTION;
			case 18:
				return TEST_INPROGRESS_INSTRUCTION;
			case 19:
				return TEST_RESULT_INSTRUCTION;
			case 20:
				return TEST_PRE_TEST5_INSTRUCTION;
			case 21:
				return TEST_INPROGRESS_INSTRUCTION;
			case 22:
				return TEST_RESULT_INSTRUCTION;
			case 23:
				return TEST_PRE_TEST1_INSTRUCTION;
			case 24:
				return TEST_INPROGRESS_INSTRUCTION;
			case 25:
				return TEST_RESULT_INSTRUCTION;
			case 26:
				return TEST_PRE_TEST2_INSTRUCTION;
			case 27:
				return TEST_INPROGRESS_INSTRUCTION;
			case 28:
				return TEST_RESULT_INSTRUCTION;
			default:
				return TEST_ERROR_INSTRUCTION;
			}
		}
	}
	
	/*
	 * returns the test command that will be written to the CommandFile.txt for the appropriate stage of the test
	 */
	public static String getTestPhaseCommand() {
		if(isDemo) {
			return TEST_FLOW_CODE_DEMO;
		} else {
			return TEST_FLOW_CODE_NORMAL[testNumber-1];
		}
	}
	
	private static void writeCommandFile(String command) {
		TestCommand newCommand = new TestCommand(command);
		newCommand.createFile();
		Log.v("TESTPROCEDURE", "[TestProcedure] Command: " + command + " has been written.");
	}
	
	/*
	 * Writes the appropriate test command, at the appropriate testPhases
	 */
	public static void writeTestPhaseCommand() {
		switch(testPhase) {
		case 0:
			if(!isCalibrated) {
				writeCommandFile(CALIBRATION_CODE);
			}
			break;
		case 3:
		case 6:
		case 9:
		case 12:
		case 15:
		case 18:
		case 21:
		case 24:
		case 27:
			writeCommandFile(getTestPhaseCommand());
			break;
		}
	}
	
	public static ArrayList<String> readDataFromFile(File file) {
		ArrayList<String> listOfData = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String linebuffer;
			while((linebuffer = reader.readLine()) != null) {
				listOfData.addAll(Arrays.asList(linebuffer.split(",")));
				Log.v("FILE", "[TestProcedure] Current line: " + linebuffer);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			Log.v("FILE", "[TestProcedure] Could not find file: " + file.getName());
		} catch (IOException ioe) {
			Log.v("FILE", "[TestProcedure] IOException: " + Arrays.toString(ioe.getStackTrace()));
		}
		if(listOfData.size() == 0) {
			Log.v("FILE", "[TestProcedure] Datafile is empty.");
		}
		Log.v("FILE", "[TestProcedure] List size: " + listOfData.size() + " Content: " + listOfData.toString());
		return listOfData;
	}
	
	public static ArrayList<ArrayList<Float>> parseGaitData (ArrayList<String> listOfData) {
		//demo parser here
		ArrayList<ArrayList<Float>> listOfGaitValues = new ArrayList<ArrayList<Float>>(2);
		listOfGaitValues.add(new ArrayList<Float>());
		listOfGaitValues.add(new ArrayList<Float>());
		float x, y;
		for(int i = 1, j = 2; i < listOfData.size()-1; i+=2, j+=2) {
			Log.v("DEMO", "[TestProcedure] x = " + listOfData.get(i) + " y = " + listOfData.get(j));
			x = Float.parseFloat(listOfData.get(i));
			y = Float.parseFloat(listOfData.get(j));
			listOfGaitValues.get(0).add(x);
			listOfGaitValues.get(1).add(y);
		}
		Log.v("FILE", "[TestProcedure] x values: " + listOfGaitValues.get(0).size() + " y values: " + listOfGaitValues.get(1).size());
		Log.v("FILE", "[TestProcedure] Parsed Demo values: " + listOfGaitValues);
		return listOfGaitValues;
	}
	
	/*
	 * Given a TestSession and a list of data strings, parse the strings and insert them into the appropriate structure
	 */
	public static void readDataIntoStructure(TestSession session, ArrayList<String> listOfData) {
		String currentCmd = getTestPhaseCommand();
		Test1 temp1O = new Test1();
		Test1 temp1C = new Test1();
		Test2 temp2 = new Test2();
		Test3 temp3 = new Test3();
		Test4 temp4 = new Test4();
		Test5 temp5O = new Test5();
		Test5 temp5C = new Test5();
		//parse the file
		if(isDemo){
			TestSession.setArrayOfDemoGaitValues(parseGaitData(listOfData));
		} else {
			if(currentCmd.equals("01")) {
				float[] arrayTempO = new float[21];
				float[] arrayTempC = new float[21];
				for(int i = 0, j = 23; i < 21; i++, j++) {
					arrayTempO[i] = Float.parseFloat(listOfData.get(i));
					arrayTempC[i] = Float.parseFloat(listOfData.get(j));
				}
				temp1O = new Test1(true, arrayTempO[0], arrayTempO[1], arrayTempO[2]
						, arrayTempO[3], arrayTempO[4], arrayTempO[5], arrayTempO[6]
						, arrayTempO[7], arrayTempO[8], arrayTempO[9], arrayTempO[10]
						, arrayTempO[11], arrayTempO[12], arrayTempO[13], arrayTempO[14]
						, arrayTempO[15], arrayTempO[16], arrayTempO[17], arrayTempO[18]
						, arrayTempO[19], arrayTempO[20]);
				temp1C = new Test1(false, arrayTempC[0], arrayTempC[1], arrayTempC[2]
						, arrayTempC[3], arrayTempC[4], arrayTempC[5], arrayTempC[6]
						, arrayTempC[7], arrayTempC[8], arrayTempC[9], arrayTempC[10]
						, arrayTempC[11], arrayTempC[12], arrayTempC[13], arrayTempC[14]
						, arrayTempC[15], arrayTempC[16], arrayTempC[17], arrayTempC[18]
						, arrayTempC[19], arrayTempC[20]);
				Log.v("DATA", "[TestProcedure] arrayTempO: " + Arrays.toString(arrayTempO));
				Log.v("DATA", "[TestProcedure] arrayTempC: " + Arrays.toString(arrayTempC));
			} else if(currentCmd.equals("02")) {
				float[] arrayTemp = new float[21];
				for(int i = 0; i < 21; i++) {
					arrayTemp[i] = Float.parseFloat(listOfData.get(i));
				}
				temp2 = new Test2(arrayTemp[0], arrayTemp[1], arrayTemp[2]
						, arrayTemp[3], arrayTemp[4], arrayTemp[5], arrayTemp[6]
						, arrayTemp[7], arrayTemp[8], arrayTemp[9], arrayTemp[10]
						, arrayTemp[11], arrayTemp[12], arrayTemp[13], arrayTemp[14]
						, arrayTemp[15], arrayTemp[16], arrayTemp[17], arrayTemp[18]
						, arrayTemp[19], arrayTemp[20]);
				Log.v("DATA", "[TestProcedure] arrayTemp: " + Arrays.toString(arrayTemp));
			} else if(currentCmd.equals("03")) {
				float[] arrayTemp = new float[21];
				for(int i = 0; i < 21; i++) {
					arrayTemp[i] = Float.parseFloat(listOfData.get(i));
				}
				temp3 = new Test3(arrayTemp[0], arrayTemp[1], arrayTemp[2]
						, arrayTemp[3], arrayTemp[4], arrayTemp[5], arrayTemp[6]
						, arrayTemp[7], arrayTemp[8], arrayTemp[9], arrayTemp[10]
						, arrayTemp[11], arrayTemp[12], arrayTemp[13], arrayTemp[14]
						, arrayTemp[15], arrayTemp[16], arrayTemp[17], arrayTemp[18]
						, arrayTemp[19], arrayTemp[20]);
				Log.v("DATA", "[TestProcedure] arrayTemp: " + Arrays.toString(arrayTemp));
			} else if(currentCmd.equals("04")) {
				float[] arrayTemp = new float[4];
				for(int i = 0; i < 4; i++) {
					arrayTemp[i] = Float.parseFloat(listOfData.get(i));
				}
				temp4 = new Test4(arrayTemp[0], arrayTemp[1], arrayTemp[2]
						, arrayTemp[3]);
				Log.v("DATA", "[TestProcedure] arrayTemp: " + Arrays.toString(arrayTemp));
			} else if(currentCmd.equals("05")) {
				float[] arrayTempO = new float[3];
				float[] arrayTempC = new float[3];
				for(int i = 0, j = 5; i < 3; i++, j++) {
					arrayTempO[i] = Float.parseFloat(listOfData.get(i));
					arrayTempC[i] = Float.parseFloat(listOfData.get(j));
				}
				temp5O = new Test5(true, arrayTempO[0], arrayTempO[1], arrayTempO[2]);
				temp5C = new Test5(false, arrayTempC[0], arrayTempC[1], arrayTempC[2]);
				Log.v("DATA", "[TestProcedure] arrayTempO: " + Arrays.toString(arrayTempO));
				Log.v("DATA", "[TestProcedure] arrayTempC: " + Arrays.toString(arrayTempC));
			}
			//set the test to the appropriate structure location
			switch(testNumber) {
			case 1:
				session.setTest1o(temp1O);
				session.setTest1c(temp1C);
				break;
			case 2:
				session.setTest2(temp2);
				break;
			case 3:
				session.setTest3(temp3);
				break;
			case 4:
				session.setTest4o(temp1O);
				session.setTest4c(temp1C);
				break;
			case 5:
				session.setTest5(temp4);
				break;
			case 6:
				session.setTest6o(temp1O);
				session.setTest6c(temp1C);
				break;
			case 7:
				session.setTest7o(temp5O);
				session.setTest7c(temp5C);
				break;
			case 8:
				session.setTest8o(temp1O);
				session.setTest8c(temp1C);
				break;
			case 9:
				session.setTest9(temp2);
				break;
			default:
				Log.v("DATA", "[TestProcedure] testNumber: " + testNumber + " is invalid.");
			}
		}
	}

	public static boolean isCalibrated() {
		return isCalibrated;
	}

	public static void setCalibrated(boolean isCalibrated) {
		TestProcedure.isCalibrated = isCalibrated;
	}
}
