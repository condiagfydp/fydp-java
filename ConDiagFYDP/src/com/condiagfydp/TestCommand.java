package com.condiagfydp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.os.Environment;
import android.util.Log;

public class TestCommand extends Activity{
	private String filename;
	private String content;
	private static ArrayList<String> listOfCommands = null;
	private static ArrayList<String> listOfCompletedCommands = new ArrayList<String>();
	
	public static final String TEST_COMMAND_FOLDERNAME = "TestCommands";
	public static final String TEST_COMMAND_FILENAME = "CommandFile.txt";
	public static final String TEST_DATA_FOLDERNAME = "TestData";
	public static final String TEST_DATA_FILENAME = "result";
	public static final String TEST_DATA_FILETYPE = ".csv";
	public static final String SAVED_DATA_FILENAME = "DataFile";
	public static final String SAVED_DATA_FILETYPE = ".txt";
	public static final String LAST_TEST_CODE = "08";
	public static final String FIRST_TEST_CODE = "10";
	public static final String CONCUSSED_DATA_FILENAME = "concussed gait";
	
	public TestCommand(String content) {
		this.filename = TEST_COMMAND_FILENAME;
		this.content = content;
	}
	
	//Creates the command file
	public File createFile() {
		//create test file
		try
	    {
	        File root = new File(Environment.getExternalStorageDirectory(), TEST_COMMAND_FOLDERNAME);
	        if (!root.exists()) {
	            root.mkdirs();
	        }
	        File gpxfile = new File(root, filename);
	        FileWriter writer = new FileWriter(gpxfile);
	        writer.append(content);
	        writer.flush();
	        writer.close();
//	        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
	    }
	    catch(IOException e)
	    {
	    	Log.v("FILE", e.getStackTrace().toString());
	    }
		File file = new File(new File(Environment.getExternalStorageDirectory(), TEST_COMMAND_FOLDERNAME), filename);
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e1) {
			Log.v("FILE", Arrays.toString(e1.getStackTrace()));
		}
		try {
			Log.v("FILE", "[TestCommand] File Location: " + file.getPath() + "Filename: " + file.getName() + " Content: " + bufferedReader.readLine());
			bufferedReader.close();
		} catch (IOException e) {
			Log.v("FILE", Arrays.toString(e.getStackTrace()));
		}
		return file;
	}
	
	//send the command
	public static void deleteCommand(File file) {
		Log.v("FILE", "[TestCommand] Time to delete the file.");
		if(file.delete()){
			Log.v("FILE", "[TestCommand] File was deleted.");
		} else {
			Log.v("FILE", "[TestCommand] File not deleted.");
		}
	}

	public static ArrayList<String> getListOfCommands() {
		return listOfCommands;
	}

	public static void setListOfCommands(ArrayList<String> listOfCommands) {
		TestCommand.listOfCommands = listOfCommands;
	}

	public static ArrayList<String> getListOfCompletedCommands() {
		return listOfCompletedCommands;
	}

	public static void setListOfCompletedCommands(
			ArrayList<String> listOfCompletedCommands) {
		TestCommand.listOfCompletedCommands = listOfCompletedCommands;
	}
	
	public static void copy(File src, File dst) {
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new FileInputStream(src);
		} catch (FileNotFoundException fnfe) {
			Log.v("FILE", "[TestCommand] File: " + src.getName() + " cannot be found.");
		}
		try {
			out = new FileOutputStream(dst);
		} catch (FileNotFoundException fnfe) {
			Log.v("FILE", "[TestCommand] File: " + src.getName() + " cannot be found.");
		}

	    // Transfer bytes from in to out
	    byte[] buf = new byte[1024];
	    int len;
	    try {
		    while ((len = in.read(buf)) > 0) {
		    	try {
		    		out.write(buf, 0, len);
		    	} catch (IndexOutOfBoundsException ioobe) {
		    		Log.v("FILE", "[TestCommand] Index out of bounds exception occured while copying the file.");
		    	}
		    }
		    in.close();
		    out.close();
	    } catch (IOException ioe) {
    		ioe.printStackTrace();
    	}
	}
}
