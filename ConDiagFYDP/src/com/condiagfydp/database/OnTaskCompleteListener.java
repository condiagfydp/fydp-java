package com.condiagfydp.database;



public interface OnTaskCompleteListener {
    // Notifies about task completeness
    void onTaskComplete(TaskBase<?,?> task);
}