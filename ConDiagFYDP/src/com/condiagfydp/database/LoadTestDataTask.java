package com.condiagfydp.database;

import android.content.res.Resources;
import android.util.Log;

public final class LoadTestDataTask extends TaskBase<Void, String> {

	private String profileID;
	private final String DB_COMMAND = "getPatientTestData";
	private final String message = "Loading Patient Test Data...";
	
	/* UI Thread */
	public LoadTestDataTask(Resources resources, String profileID) {
		super(resources);
		this.profileID = profileID;
		mProgressMessage = message;
	}    

	/* Separate Thread */
	@Override
	protected String doInBackground(Void... args) {
		String result = null;
		if (isCancelled()) {
			result = "cancelled";
			Log.v("DATABASE", "[LoadTestDataTask] Query cancelled.");
		}
		try {
			String s = null;
			s = Networks.HOST + "?message1=" + DB_COMMAND + "&message2=" + profileID;
			Log.v("DATABASE", "[LoadTestDataTask] DB Call: " + s);
			result = Networks.httpClient(s);
		} 
		catch (Exception e) {
			Log.v("DATABASE", "Error: " + e.getStackTrace().toString());
			e.printStackTrace();
			return null;
		}
		return result;
	}
}