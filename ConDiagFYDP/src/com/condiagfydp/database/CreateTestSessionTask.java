package com.condiagfydp.database;

import android.content.res.Resources;
import android.util.Log;

public final class CreateTestSessionTask extends TaskBase<Void, String> {

	private String profileID;
	private String date;
	private final String DB_COMMAND = "createNewPatientTestsSession";
	private final String message = "Creating Test Session...";
	
	/* UI Thread */
	public CreateTestSessionTask(Resources resources, String profileID, String date) {
		super(resources);
		this.profileID = profileID;
		this.date = date;
		mProgressMessage = message;
	}    

	/* Separate Thread */
	@Override
	protected String doInBackground(Void... args) {
		String result = null;
		if (isCancelled()) {
			result = "cancelled";
		}
		try {
			String s = null;
			s = Networks.HOST + "?message1=" + DB_COMMAND + "&message2=" + profileID + "&message3=" + date;
			Log.v("DATABASE", "DB Call: " + s);
			result = Networks.httpClient(s);
		} 
		catch (Exception e) {
			Log.v("DATABASE", "Error: " + e.getStackTrace().toString());
			e.printStackTrace();
			return null;
		}
		return result;
	}
}