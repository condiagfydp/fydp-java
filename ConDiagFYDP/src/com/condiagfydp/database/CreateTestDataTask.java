package com.condiagfydp.database;

import android.content.res.Resources;
import android.util.Log;

public final class CreateTestDataTask extends TaskBase<Void, String> {

	private String sessionID;
	private String testType;
	private String meanX;
	private String meanY;
	private String stdX;
	private String stdY;
	private String kurtosisX;
	private String kurtosisY;
	private String covarianceXY;
	private String stdVX;
	private String stdVY;
	private String kurtosisVX;
	private String kurtosisVY;
	private String covarianceVXY;
	private String rmsVX;
	private String rmsVY;
	private String stdAX;
	private String stdAY;
	private String kurtosisAX;
	private String kurtosisAY;
	private String covarianceAXY;
	private String rmsAX;
	private String rmsAY;
	private String minX;
	private String maxX;
	private String minY;
	private String maxAccel;
	private String meanPowerFreq;
	private String peakPowerFreq;
	private String highEnergy;
	private final String DB_COMMAND = "createNewTestData";
	private final String message = "Creating Test Data...";
	
	/* UI Thread */
	public CreateTestDataTask(Resources resources, String[] testData) {
		super(resources);
		this.sessionID = testData[0];
		this.testType = testData[1];
		this.meanX = testData[2];
		this.meanY = testData[3];
		this.stdX = testData[4];
		this.stdY = testData[5];
		this.kurtosisX = testData[6];
		this.kurtosisY = testData[7];
		this.covarianceXY = testData[8];
		this.stdVX = testData[9];
		this.stdVY = testData[10];
		this.kurtosisVX = testData[11];
		this.kurtosisVY = testData[12];
		this.covarianceVXY = testData[13];
		this.rmsVX = testData[14];
		this.rmsVY = testData[15];
		this.stdAX = testData[16];
		this.stdAY = testData[17];
		this.kurtosisAX = testData[18];
		this.kurtosisAY = testData[19];
		this.covarianceAXY = testData[20];
		this.rmsAX = testData[21];
		this.rmsAY = testData[22];
		this.minX = testData[23];
		this.maxX = testData[24];
		this.minY = testData[25];
		this.maxAccel = testData[26];
		this.meanPowerFreq = testData[27];
		this.peakPowerFreq = testData[28];
		this.highEnergy = testData[29];
		mProgressMessage = message;
	}    

	/* Separate Thread */
	@Override
	protected String doInBackground(Void... args) {
		String result = null;
		if (isCancelled()) {
			result = "cancelled";
		}
		try {
			String s = null;
			s = Networks.HOST + "?message1=" + DB_COMMAND + "&message2=" + sessionID + "&message3=" + testType + "&message4=" 
					+ meanX + "&message5=" + meanY + "&message6=" + stdX + "&message7=" + stdY + "&message8=" + kurtosisX
					+ "&message9=" + kurtosisY + "&message10=" + covarianceXY + "&message11=" + stdVX + "&message12=" + stdVY
					+ "&message13=" + kurtosisVX + "&message14=" + kurtosisVY + "&message15=" + covarianceVXY + "&message16=" + rmsVX
					+ "&message17=" + rmsVY + "&message18=" + stdAX + "&message19=" + stdAY + "&message20=" + kurtosisAX
					+ "&message21=" + kurtosisAY + "&message22=" + covarianceAXY + "&message23=" + rmsAX + "&message24=" + rmsAY
					+ "&message25=" + minX + "&message26=" + maxX + "&message27=" + minY + "&message28=" + maxAccel
					+ "&message29=" + meanPowerFreq + "&message30=" + peakPowerFreq + "&message31=" + highEnergy;
			Log.v("DATABASE", "DB Call: " + s);
			result = Networks.httpClient(s);
		} 
		catch (Exception e) {
			Log.v("DATABASE", "Error: " + e.getStackTrace().toString());
			e.printStackTrace();
			return null;
		}
		return result;
	}
}