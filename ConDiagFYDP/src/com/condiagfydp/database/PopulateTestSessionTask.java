package com.condiagfydp.database;

import java.util.ArrayList;

import android.content.res.Resources;
import android.util.Log;

public final class PopulateTestSessionTask extends TaskBase<Void, String> {

	private String sessionID;
	private String test1o;
	private String test1c;
	private String test2;
	private String test3;
	private String test4o;
	private String test4c;
	private String test5;
	private String test6o;
	private String test6c;
	private String test7o;
	private String test7c;
	private String test8o;
	private String test8c;
	private String test9;
	private final String DB_COMMAND = "populateNewPatientTestsSession";
	private final String message = "Populating Test Session with Test IDs...";
	
	/* UI Thread */
	public PopulateTestSessionTask(Resources resources, String sessionID, ArrayList<String> listOfTestIDs) {
		super(resources);
		this.sessionID = sessionID;
		this.test1o = listOfTestIDs.get(0);
		this.test1c = listOfTestIDs.get(1);
		this.test2 = listOfTestIDs.get(2);
		this.test3 = listOfTestIDs.get(3);
		this.test4o = listOfTestIDs.get(4);
		this.test4c = listOfTestIDs.get(5);
		this.test5 = listOfTestIDs.get(6);
		this.test6o = listOfTestIDs.get(7);
		this.test6c = listOfTestIDs.get(8);
		this.test7o = listOfTestIDs.get(9);
		this.test7c = listOfTestIDs.get(10);
		this.test8o = listOfTestIDs.get(11);
		this.test8c = listOfTestIDs.get(12);
		this.test9 = listOfTestIDs.get(13);
		mProgressMessage = message;
	}    

	/* Separate Thread */
	@Override
	protected String doInBackground(Void... args) {
		String result = null;
		if (isCancelled()) {
			result = "cancelled";
		}
		try {
			String s = null;
			s = Networks.HOST + "?message1=" + DB_COMMAND + "&message2=" + sessionID + "&message3=" + test1o + "&message4=" 
					+ test1c + "&message5=" + test2 + "&message6=" + test3 + "&message7=" + test4o + "&message8=" + test4c
					+ "&message9=" + test5 + "&message10=" + test6o + "&message11=" + test6c + "&message12=" + test7o
					+ "&message13=" + test7c + "&message14=" + test8o + "&message15=" + test8c + "&message16=" + test9;
			Log.v("DATABASE", "[PopulateTestSessionTask] DB Call: " + s);
			result = Networks.httpClient(s);
		} 
		catch (Exception e) {
			Log.v("DATABASE", "[PopulateTestSessionTask] Error: " + e.getStackTrace().toString());
			e.printStackTrace();
			return null;
		}
		return result;
	}
}