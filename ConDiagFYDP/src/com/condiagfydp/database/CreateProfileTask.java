package com.condiagfydp.database;

import android.content.res.Resources;
import android.util.Log;

public final class CreateProfileTask extends TaskBase<Void, String> {

	private String age;
	private String height;
	private String weight;
	private String sport;
	private String gender;
	private String units;
	private final String DB_COMMAND = "createNewProfile";
	private final String message = "Creating Patient Profile...";
	
	/* UI Thread */
	public CreateProfileTask(Resources resources, String age, String height, String weight, String sport, String gender, String units) {
		super(resources);
		this.age = age;
		this.height = height;
		this.weight = weight;
		this.sport = sport;
		this.gender = gender;
		this.units = units;
		mProgressMessage = message;
	}    

	/* Separate Thread */
	@Override
	protected String doInBackground(Void... args) {
		String result = null;
		if (isCancelled()) {
			result = "cancelled";
		}
		try {
			String s = null;
			s = Networks.HOST + "?message1=" + DB_COMMAND + "&message2=" + age + "&message3=" + height + "&message4=" 
					+ weight + "&message5=" + sport + "&message6=" + gender + "&message7=" + units;
			Log.v("DATABASE", "DB Call: " + s);
			result = Networks.httpClient(s);
		} 
		catch (Exception e) {
			Log.v("DATABASE", "Error: " + e.getStackTrace().toString());
			e.printStackTrace();
			return null;
		}
		return result;
	}
}