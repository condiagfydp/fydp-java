package com.condiagfydp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.condiagfydp.database.AsyncTaskManager;
import com.condiagfydp.database.LoadProfileTask;
import com.condiagfydp.database.OnTaskCompleteListener;
import com.condiagfydp.database.TaskBase;

public class PatientSelectionActivity extends Activity implements
		ActionBar.OnNavigationListener, OnTaskCompleteListener {

	private String selectedPatientID;
	private String selectedPatientName;
	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * current dropdown position.
	 */
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	private AsyncTaskManager taskManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_selection);

		taskManager = new AsyncTaskManager(this, this);
		
		// Set up the action bar to show a dropdown list.
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		// Show the Up button in the action bar.
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Set up the dropdown list navigation in the action bar.
		actionBar.setListNavigationCallbacks(
		// Specify a SpinnerAdapter to populate the dropdown list.
				new ArrayAdapter<String>(actionBar.getThemedContext(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, new String[] {
								getString(R.string.title_PatientSelection),
								getString(R.string.title_MainMenu), }), this);
		
		LinearLayout ll = (LinearLayout) findViewById(R.id.patientList);
		
		//populate the list with the list of patients stored locally
		for(String patientName : PatientList.getListOfPatients()) {
			Button patientButton = new Button(this);
			patientButton.setText(patientName);
			ll.addView(patientButton);
			final String fPatientName = patientName;
			patientButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					selectedPatientID = PatientList.getPatientID(fPatientName);
					selectedPatientName = fPatientName;
					viewPatientProfile(view);
				}
			});
		}
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current dropdown position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current dropdown position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_selection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(int position, long id) {
//		Log.v("NAV", "navigation item: " + position);
		switch (position) {
			case 0:
				//Do Nothing
				break;
			case 1:
				Intent intent = new Intent(this, MainMenuActivity.class);
				startActivity(intent);
				break;
			default:
				//do nothing
				break;
		}
		return true;
	}
	
	public void viewPatientProfile(View view) {
		PatientProfile.setPatientID(selectedPatientID);
		PatientProfile.setName(selectedPatientName);
		try {
    		Log.v("DATABASE", "Sending Patient ID: " + PatientProfile.getPatientID());
    		taskManager.setupTask(new LoadProfileTask(getResources(), PatientProfile.getPatientID()), true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * This handles the behaviour after the results from the DB query is received
	 * @see com.condiagfydp.database.OnTaskCompleteListener#onTaskComplete(com.condiagfydp.database.TaskBase)
	 */
	@Override
	public void onTaskComplete(TaskBase<?, ?> task) {
		if(task.getClass() == LoadProfileTask.class){
			Object o = null;
			try {
				o = task.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.v("DATABASE", "Received: " + (String) task.mResult);
			if (o != null){
				String result = (String) task.mResult;
				Log.v("DATA", "[PatientSelectionActivity] " + result.charAt(0));
				if(result.charAt(0) == ' ') {
					Toast.makeText(getApplicationContext(), "This patient's profile is corrupted or does not exist.", Toast.LENGTH_LONG).show();
				} else {
					PatientProfile.setAge(Integer.parseInt(result.split(" ")[1]));
					if(result.split(" ")[2].equals("m")) {
						PatientProfile.setMale(true);
					} else {
						PatientProfile.setMale(false);
					}
					PatientProfile.setSport(result.split(" ")[3]);
					if(result.split(" ")[4].equals("m")) {
						PatientProfile.setMetric(true);
					} else {
						PatientProfile.setMetric(false);
					}
					Log.v("DATA", "Height: " + result.split(" ")[5]);
					PatientProfile.setHeight(Integer.parseInt(result.split(" ")[5].trim()));
					Log.v("DATA", "Weight: " + result.split(" ")[6]);
					PatientProfile.setWeight(Integer.parseInt(result.split(" ")[6].trim()));
					Intent intent = new Intent(this, PatientProfileActivity.class);
			    	startActivity(intent);
				}
			}
			else {
			}
		}
	}

}
