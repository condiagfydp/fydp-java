package com.condiagfydp;

import java.util.ArrayList;

import android.app.Activity;

public class PatientProfile extends Activity{
	private static String name = null;
	private static int age = 0;
	private static int height = 0;
	private static int weight = 0;
	private static String sport = null;
	private static boolean isMale = true;
	private static boolean isMetric = true;
	private static String profileID = "";
	private static ArrayList<TestSession> listOfTestSessions;

	public static void createPatientProfile(String name, int age, int height, int weight, String sport, boolean isMale, boolean isMetric) {
		setName(name);
		setAge(age);
		setHeight(height);
		setWeight(weight);
		setSport(sport);
		setMale(isMale);
		setMetric(isMetric);
		listOfTestSessions = null;
	}
	
	public static String getName() {
		return name;
	}


	public static void setName(String name) {
		PatientProfile.name = name;
	}


	public static int getAge() {
		return age;
	}


	public static void setAge(int age) {
		PatientProfile.age = age;
	}


	public static int getHeight() {
		return height;
	}

	public static String getIHeight() {
		int feet = (int)Math.floor((double)height*0.0328084);
		int inches = (int)Math.round(((double)height*0.0328084 - feet)*12);
		return feet + "\'" + inches + "\"";
	}

	public static void setHeight(int height) {
		PatientProfile.height = height;
	}

	public static int getIWeight() {
		return (int)Math.round((double)weight*2.20462);
	}
	
	public static int getWeight() {
		return weight;
	}


	public static void setWeight(int weight) {
		PatientProfile.weight = weight;
	}


	public static String getSport() {
		return sport;
	}


	public static void setSport(String sport) {
		PatientProfile.sport = sport;
	}


	public static boolean isMale() {
		return isMale;
	}

	public static String getGender() {
		if(isMale) {
			return "Male";
		} else {
			return "Female";
		}
	}

	public static void setMale(boolean isMale) {
		PatientProfile.isMale = isMale;
	}


	public static boolean isMetric() {
		return isMetric;
	}


	public static void setMetric(boolean isMetric) {
		PatientProfile.isMetric = isMetric;
	}


	public static String getPatientID() {
		return profileID;
	}


	public static void setPatientID(String patientID) {
		PatientProfile.profileID = patientID;
	}

	public static ArrayList<TestSession> getListOfTestSessions() {
		return listOfTestSessions;
	}

	public static void setListOfTestSessions(
			ArrayList<TestSession> listOfTestSessions) {
		PatientProfile.listOfTestSessions = listOfTestSessions;
	}
	
}
