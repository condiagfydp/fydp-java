package com.condiagfydp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.os.Environment;
import android.util.Log;

public class PatientList extends Activity {

	private static Map<String, String> mapOfPatients;
	public static final String PATIENT_LIST_FOLDERNAME = "PatientList";
	public static final String PATIENT_LIST_FILENAME = "PatientList.txt";
	
	/*
	 * creates a new folder and file to store patient list (if it doesnt exist already), return the file containing the patient list
	 */
	private static File createFile() {
		File root = new File(Environment.getExternalStorageDirectory(), PATIENT_LIST_FOLDERNAME);
        if (!root.exists()) {
            root.mkdirs();
        }
        File file = new File(root, PATIENT_LIST_FILENAME);
        if(!file.exists()) {
        	try {
	        	FileWriter writer = new FileWriter(file);
	            writer.flush();
	            writer.close();
	            Log.v("FILE", "Patient list does not exist, a new list has been created.");
        	} catch (IOException e) {
        		Log.v("FILE", "Error in creating file: " + e.getStackTrace().toString());
        	}
        } else {
        	Log.v("FILE", "Patient list already exist.");
        }
        return file;
	}
	
	public static void addNewPatient(String patientName, String profileID) {
		mapOfPatients.put(patientName, profileID);
		Log.v("DATA", "New patient: " + patientName + " has been added.");
	}
	
	public static void removePatient(String patientName) {
		mapOfPatients.remove(patientName);
		Log.v("DATA", "Patient: " + patientName + " has been removed.");
		writePatientList();
	}
	
	/*
	 * reads the patient list text file, if it is empty then return -1, otherwise, populate the map of patients and return 0;
	 */
	public static int readPatientList() {
		mapOfPatients = new HashMap<String, String>();
		File patientList = createFile();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(patientList));
			String line;
			while((line = reader.readLine()) != null) {
				try{
					Log.v("DATA", "Patient List current line: " + line);
					Log.v("DATA", "Patient Name: " + line.substring(0, line.indexOf(",")));
					Log.v("DATA", "Patient ID: " + line.substring(line.indexOf(",")+2));
					mapOfPatients.put(line.substring(0, line.indexOf(",")),line.substring(line.indexOf(",")+2));
				} catch (Exception e) {
					Log.v("DATA", "Patient list is corrupted or contains invalid data.");
					e.printStackTrace();
				}
			}
			reader.close();
			Log.v("DATA", "Patient List: " + mapOfPatients.toString());
		} catch (IOException e) {
			Log.v("FILE", "Error with reading patient list: " + e.getStackTrace().toString());
		}
		if(mapOfPatients.size() == 0) {
			return -1;
		} else {
			return 0;
		}
	}
	
	/*
	 * write mapOfPatients to file, returns 0 if successful, -1 if unsuccessful or map is empty.
	 */
	public static int writePatientList() {
		if(mapOfPatients.size() == 0) {
			return -1;
		} else {
			File patientList = createFile();
			try {
				BufferedWriter writer = new BufferedWriter(new FileWriter(patientList));
				for(String patient : mapOfPatients.keySet()) {
					writer.write(patient + ", " + mapOfPatients.get(patient) + "\n");
				}
				writer.flush();
				writer.close();
				Log.v("DATA", "Patient List: " + readFile());
				return 0;
			} catch (IOException e) {
				Log.v("FILE", "Error with writing patient list: " + e.getStackTrace().toString());
				return -1;
			}
		}
	}
	
	private static String readFile() {
		File patientList = createFile();
		String output = "";
		String line;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(patientList));
			while((line = reader.readLine()) != null) {
				output += line + "\n";
			}
			reader.close();
		} catch (IOException e) {
			Log.v("FILE", "Error with reading patient list: " + e.getStackTrace().toString());
		}
		return output;
	}
	
	public static Set<String> getListOfPatients() {
		return mapOfPatients.keySet();
	}
	
	public static String getPatientID(String patientName) {
		return mapOfPatients.get(patientName);
	}
}
