package com.condiagfydp.testTypes;

import android.app.Activity;

public class Test5 extends Activity {
	private final String TEST_NAME = "Oscillating Balance";
	private boolean eyesOnly;
	private float meanPowerFreq;
	private float peakPowerFreq;
	private float highEnergy;
	
	public Test5() {
		//do nothing
	}
	
	public Test5 (boolean eyesOnly, float meanPowerFreq, float peakPowerFreq, float highEnergy) {
		this.eyesOnly = eyesOnly;
		this.meanPowerFreq = meanPowerFreq;
		this.peakPowerFreq = peakPowerFreq;
		this.highEnergy = highEnergy;
	}

	public boolean isEyesOnly() {
		return eyesOnly;
	}

	public void setEyesOnly(boolean eyesOnly) {
		this.eyesOnly = eyesOnly;
	}

	public float getMeanPowerFreq() {
		return meanPowerFreq;
	}

	public void setMeanPowerFreq(float meanPowerFreq) {
		this.meanPowerFreq = meanPowerFreq;
	}

	public float getPeakPowerFreq() {
		return peakPowerFreq;
	}

	public void setPeakPowerFreq(float peakPowerFreq) {
		this.peakPowerFreq = peakPowerFreq;
	}

	public float getHighEnergy() {
		return highEnergy;
	}

	public void setHighEnergy(float highEnergy) {
		this.highEnergy = highEnergy;
	}

	public String getTEST_NAME() {
		return TEST_NAME;
	}
	
}
