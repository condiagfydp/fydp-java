package com.condiagfydp.testTypes;

import android.app.Activity;

public class Test4 extends Activity {
	private final String TEST_NAME = "Gait Initiation";
	private float minX;
	private float maxX;
	private float minY;
	private float maxAccel;
	
	public Test4() {
		//do nothing
	}
	
	public Test4 (float minX, float maxX, float minY, float maxAccel) {
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxAccel = maxAccel;
	}
	
	public String getTEST_NAME() {
		return TEST_NAME;
	}
	public float getMinX() {
		return minX;
	}
	public void setMinX(float minX) {
		this.minX = minX;
	}
	public float getMaxX() {
		return maxX;
	}
	public void setMaxX(float maxX) {
		this.maxX = maxX;
	}
	public float getMinY() {
		return minY;
	}
	public void setMinY(float minY) {
		this.minY = minY;
	}
	public float getMaxAccel() {
		return maxAccel;
	}
	public void setMaxAccel(float maxAccel) {
		this.maxAccel = maxAccel;
	}
}
