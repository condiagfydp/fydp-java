package com.condiagfydp.testTypes;

import android.app.Activity;

public class Test1 extends Activity{
	private final String TEST_NAME = "Static Balance";
	private boolean eyesOpen;
	private float meanX;
	private float meanY;
	private float stdX;
	private float stdY;
	private float kurtosisX;
	private float kurtosisY;
	private float covarianceXY;
	private float stdVX;
	private float stdVY;
	private float kurtosisVX;
	private float kurtosisVY;
	private float covarianceVXY;
	private float rmsVX;
	private float rmsVY;
	private float stdAX;
	private float stdAY;
	private float kurtosisAX;
	private float kurtosisAY;
	private float covarianceAXY;
	private float rmsAX;
	private float rmsAY;
	
	public Test1() {
		//do nothing
	}
	
	public Test1 (boolean eyesOpen, float meanX, float meanY, float stdX, float stdY, float kurtosisX, float kurtosisY, float covarianceXY
			,float stdVX, float stdVY, float kurtosisVX, float kurtosisVY, float covarianceVXY, float rmsVX, float rmsVY
			,float stdAX, float stdAY, float kurtosisAX, float kurtosisAY, float covarianceAXY, float rmsAX, float rmsAY) {
		this.eyesOpen = eyesOpen;
		this.meanX = meanX;
		this.meanY = meanY;
		this.stdX = stdX;
		this.stdY = stdY;
		this.kurtosisX = kurtosisX;
		this.kurtosisY = kurtosisY;
		this.covarianceXY = covarianceXY;
		this.stdVX = stdVX;
		this.stdVY = stdVY;
		this.kurtosisVX = kurtosisVX;
		this.kurtosisVY = kurtosisVY;
		this.covarianceVXY = covarianceVXY;
		this.rmsVX = rmsVX;
		this.rmsVY = rmsVY;
		this.stdAX = stdAX;
		this.stdAY = stdAY;
		this.kurtosisAX = kurtosisAX;
		this.kurtosisAY = kurtosisAY;
		this.covarianceAXY = covarianceAXY;
		this.rmsAX = rmsAX;
		this.rmsAY = rmsAY;
	}
	public String getTEST_NAME() {
		return TEST_NAME;
	}
	public float getMeanX() {
		return meanX;
	}
	public void setMeanX(float meanX) {
		this.meanX = meanX;
	}
	public float getMeanY() {
		return meanY;
	}
	public void setMeanY(float meanY) {
		this.meanY = meanY;
	}
	public float getStdX() {
		return stdX;
	}
	public void setStdX(float stdX) {
		this.stdX = stdX;
	}
	public float getStdY() {
		return stdY;
	}
	public void setStdY(float stdY) {
		this.stdY = stdY;
	}
	public float getKurtosisX() {
		return kurtosisX;
	}
	public void setKurtosisX(float kurtosisX) {
		this.kurtosisX = kurtosisX;
	}
	public float getKurtosisY() {
		return kurtosisY;
	}
	public void setKurtosisY(float kurtosisY) {
		this.kurtosisY = kurtosisY;
	}
	public float getCovarianceXY() {
		return covarianceXY;
	}
	public void setCovarianceXY(float covarianceXY) {
		this.covarianceXY = covarianceXY;
	}
	public boolean isEyesOpen() {
		return eyesOpen;
	}
	public void setEyesOpen(boolean eyesOpen) {
		this.eyesOpen = eyesOpen;
	}
	public float getStdVX() {
		return stdVX;
	}
	public void setStdVX(float stdVX) {
		this.stdVX = stdVX;
	}
	public float getStdVY() {
		return stdVY;
	}
	public void setStdVY(float stdVY) {
		this.stdVY = stdVY;
	}
	public float getKurtosisVX() {
		return kurtosisVX;
	}
	public void setKurtosisVX(float kurtosisVX) {
		this.kurtosisVX = kurtosisVX;
	}
	public float getKurtosisVY() {
		return kurtosisVY;
	}
	public void setKurtosisVY(float kurtosisVY) {
		this.kurtosisVY = kurtosisVY;
	}
	public float getCovarianceVXY() {
		return covarianceVXY;
	}
	public void setCovarianceVXY(float covarianceVXY) {
		this.covarianceVXY = covarianceVXY;
	}
	public float getRmsVX() {
		return rmsVX;
	}
	public void setRmsVX(float rmsVX) {
		this.rmsVX = rmsVX;
	}
	public float getRmsVY() {
		return rmsVY;
	}
	public void setRmsVY(float rmsVY) {
		this.rmsVY = rmsVY;
	}
	public float getStdAX() {
		return stdAX;
	}
	public void setStdAX(float stdAX) {
		this.stdAX = stdAX;
	}
	public float getStdAY() {
		return stdAY;
	}
	public void setStdAY(float stdAY) {
		this.stdAY = stdAY;
	}
	public float getKurtosisAX() {
		return kurtosisAX;
	}
	public void setKurtosisAX(float kurtosisAX) {
		this.kurtosisAX = kurtosisAX;
	}
	public float getKurtosisAY() {
		return kurtosisAY;
	}
	public void setKurtosisAY(float kurtosisAY) {
		this.kurtosisAY = kurtosisAY;
	}
	public float getCovarianceAXY() {
		return covarianceAXY;
	}
	public void setCovarianceAXY(float covarianceAXY) {
		this.covarianceAXY = covarianceAXY;
	}
	public float getRmsAX() {
		return rmsAX;
	}
	public void setRmsAX(float rmsAX) {
		this.rmsAX = rmsAX;
	}
	public float getRmsAY() {
		return rmsAY;
	}
	public void setRmsAY(float rmsAY) {
		this.rmsAY = rmsAY;
	}
}
