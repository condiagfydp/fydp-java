package com.condiagfydp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.condiagfydp.testTypes.Test1;
import com.condiagfydp.testTypes.Test2;
import com.condiagfydp.testTypes.Test3;
import com.condiagfydp.testTypes.Test4;
import com.condiagfydp.testTypes.Test5;

public class TestSession extends Activity {
	private String profileID;
	private String sessionID;
	private ArrayList<String> testIDs;
	private String date;
	private Test1 test1o;
	private Test1 test1c;
	private Test2 test2;
	private Test3 test3;
	private Test1 test4o;
	private Test1 test4c;
	private Test4 test5;
	private Test1 test6o;
	private Test1 test6c;
	private Test5 test7o;
	private Test5 test7c;
	private Test1 test8o;
	private Test1 test8c;
	private Test2 test9;
	private static ArrayList<ArrayList<Float>> arrayOfDemoGaitValues;
	private static TestSession currentSession;
	private static final int NUMBER_OF_TEST_DATA = 28;
	
	
	@SuppressLint("SimpleDateFormat")
	public TestSession () {
		this.profileID = "";
		this.sessionID = "";
		this.testIDs = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		this.date = sdf.format(new GregorianCalendar().getTime());
	}
	
	public TestSession (String profileID, String sessionID, ArrayList<String> testIDs, String date) {
		this.profileID = profileID;
		this.sessionID = sessionID;
		this.testIDs = testIDs;
		this.date = date;
	}
	
	public TestSession (String profileID, String sessionID, ArrayList<String> testIDs, String date, Test1 test1o, Test1 test1c, Test2 test2
			, Test3 test3, Test1 test4o, Test1 test4c, Test4 test5, Test1 test6o, Test1 test6c,  Test5 test7o
			, Test5 test7c, Test1 test9o, Test1 test9c, Test2 test10) {
		this.profileID = profileID;
		this.sessionID = sessionID;
		this.testIDs = testIDs;
		this.date = date;
		this.test1o = test1o;
		this.test1c = test1c;
		this.test2 = test2;
		this.test3 = test3;
		this.test4o = test4o;
		this.test4c = test4c;
		this.test5 = test5;
		this.test6o = test6o;
		this.test6c = test6c;
		this.test7o = test7o;
		this.test7c = test7c;
		this.test8o = test9o;
		this.test8c = test9c;
		this.test9 = test10;
	}
	
	public static TestSession findSessionByID(String sessionID) {
		for(TestSession session : PatientProfile.getListOfTestSessions()) {
			if(session.getSessionID().equals(sessionID)) {
				return session;
			}
		}
		Log.v("DATA", "[TestSession] No session can be match to ID: " + sessionID);
		return new TestSession();
	}
	
	/*
	 * reads an array of strings and stores the data into the appropriate data structure within listOfTestSessions
	 */
	public static void readTestData(ArrayList<TestSession> listOfTestSessions, String[] arrayOfDBData) {
		int numOfTests = arrayOfDBData.length/31;
		Log.v("DATABASE", "[TestSession] Number of Test Data: " + numOfTests + " Number of array values: " + arrayOfDBData.length);
		if(arrayOfDBData.length%31 != 0) {
			Log.v("DATABASE", "[TestSession] Test data is not complete!");
		}
		for(int i = 0; i < numOfTests; i++) {
			int si = i*31; //si = startIndex
			//determine the appropriate test session
			String sessionID = arrayOfDBData[si];
			TestSession thisSession = findSessionByID(sessionID);
			if(thisSession.getSessionID().equals("")) {
				Log.v("DATA", "[TestSession] Session ID: " + sessionID + " has not been initialized.");
			}
			//determine the test type
			int testType = Integer.parseInt(arrayOfDBData[si + 2]);
			//load all the test data into an array
			float[] arrayOfTestFloats = new float[NUMBER_OF_TEST_DATA];
//			Log.v("DATABASE", "[TestSession] ArrayOfDBData: " + Arrays.toString(arrayOfDBData));
			for(int j = 0; j < NUMBER_OF_TEST_DATA; j++) {
				arrayOfTestFloats[j] = Float.parseFloat(arrayOfDBData[si+3+j]);
			}
			boolean boolIndicator;
			Test1 temp1 = new Test1();
			Test2 temp2 = new Test2();
			Test3 temp3 = new Test3();
			Test4 temp4 = new Test4();
			Test5 temp5 = new Test5();
			//parse the array into the appropriate test type object
			
			if(testType == 1 || testType == 2 || testType == 5 || testType == 6 || testType == 8 || testType == 9 || testType == 12 || testType == 13) {
				if(testType == 1 || testType == 5 || testType == 8 || testType == 12) {
					boolIndicator = true;
				} else {
					boolIndicator = false;
				}
				temp1 = new Test1(boolIndicator, arrayOfTestFloats[0], arrayOfTestFloats[1], arrayOfTestFloats[2]
						, arrayOfTestFloats[3], arrayOfTestFloats[4], arrayOfTestFloats[5], arrayOfTestFloats[6]
						, arrayOfTestFloats[7], arrayOfTestFloats[8], arrayOfTestFloats[9], arrayOfTestFloats[10]
						, arrayOfTestFloats[11], arrayOfTestFloats[12], arrayOfTestFloats[13], arrayOfTestFloats[14]
						, arrayOfTestFloats[15], arrayOfTestFloats[16], arrayOfTestFloats[17], arrayOfTestFloats[18]
						, arrayOfTestFloats[19], arrayOfTestFloats[20]);
			} else if(testType == 10 || testType == 11) {
				if(testType == 10) {
					boolIndicator = true;
				} else {
					boolIndicator = false;
				}
				temp5 = new Test5(boolIndicator, arrayOfTestFloats[25], arrayOfTestFloats[26], arrayOfTestFloats[27]);
			} else if(testType == 3 || testType == 14) {
				temp2 = new Test2(arrayOfTestFloats[0], arrayOfTestFloats[1], arrayOfTestFloats[2]
						, arrayOfTestFloats[3], arrayOfTestFloats[4], arrayOfTestFloats[5], arrayOfTestFloats[6]
						, arrayOfTestFloats[7], arrayOfTestFloats[8], arrayOfTestFloats[9], arrayOfTestFloats[10]
						, arrayOfTestFloats[11], arrayOfTestFloats[12], arrayOfTestFloats[13], arrayOfTestFloats[14]
						, arrayOfTestFloats[15], arrayOfTestFloats[16], arrayOfTestFloats[17], arrayOfTestFloats[18]
						, arrayOfTestFloats[19], arrayOfTestFloats[20]);
			} else if(testType == 4) {
				temp3 = new Test3(arrayOfTestFloats[0], arrayOfTestFloats[1], arrayOfTestFloats[2]
						, arrayOfTestFloats[3], arrayOfTestFloats[4], arrayOfTestFloats[5], arrayOfTestFloats[6]
						, arrayOfTestFloats[7], arrayOfTestFloats[8], arrayOfTestFloats[9], arrayOfTestFloats[10]
						, arrayOfTestFloats[11], arrayOfTestFloats[12], arrayOfTestFloats[13], arrayOfTestFloats[14]
						, arrayOfTestFloats[15], arrayOfTestFloats[16], arrayOfTestFloats[17], arrayOfTestFloats[18]
						, arrayOfTestFloats[19], arrayOfTestFloats[20]);
			} else if(testType == 7) {
				temp4 = new Test4(arrayOfTestFloats[21], arrayOfTestFloats[22], arrayOfTestFloats[23], arrayOfTestFloats[24]);
			} else {
				Log.v("DATA", "[TestSession] Test Type: " + testType + " is not valid.");
			}
			//store the test data to the appropriate location
			switch(testType) {
			case 1:
				if(thisSession.getTest1o() == null) {
					thisSession.setTest1o(temp1);
				} else {
					Log.v("DATA", "[TestSession] Test1o already exist.");
				}
				break;
			case 2:
				if(thisSession.getTest1c() == null) {
					thisSession.setTest1c(temp1);
				} else {
					Log.v("DATA", "[TestSession] Test1c already exist.");
				}
				break;
			case 3:
				if(thisSession.getTest2() == null) {
					thisSession.setTest2(temp2);
				} else {
					Log.v("DATA", "[TestSession] Test2 already exist.");
				}
				break;
			case 4:
				if(thisSession.getTest3() == null) {
					thisSession.setTest3(temp3);
				} else {
					Log.v("DATA", "[TestSession] Test3 already exist.");
				}
				break;
			case 5:
				if(thisSession.getTest4o() == null) {
					thisSession.setTest4o(temp1);
				} else {
					Log.v("DATA", "[TestSession] Test4o already exist.");
				}
				break;
			case 6:
				if(thisSession.getTest4c() == null) {
					thisSession.setTest4c(temp1);
				} else {
					Log.v("DATA", "[TestSession] Test4c already exist.");
				}
				break;
			case 7:
				if(thisSession.getTest5() == null) {
					thisSession.setTest5(temp4);
				} else {
					Log.v("DATA", "[TestSession] Test5 already exist.");
				}
				break;
			case 8:
				if(thisSession.getTest6o() == null) {
					thisSession.setTest6o(temp1);
				} else {
					Log.v("DATA", "[TestSession] Test6o already exist.");
				}
				break;
			case 9:
				if(thisSession.getTest6c() == null) {
					thisSession.setTest6c(temp1);
				} else {
					Log.v("DATA", "[TestSession] Test6c already exist.");
				}
				break;
			case 10:
				if(thisSession.getTest7o() == null) {
					thisSession.setTest7o(temp5);
				} else {
					Log.v("DATA", "[TestSession] Test7 already exist.");
				}
				break;
			case 11:
				if(thisSession.getTest7c() == null) {
					thisSession.setTest7c(temp5);
				} else {
					Log.v("DATA", "[TestSession] Test8 already exist.");
				}
				break;
			case 12:
				if(thisSession.getTest8o() == null) {
					thisSession.setTest8o(temp1);
				} else {
					Log.v("DATA", "[TestSession] Test9o already exist.");
				}
				break;
			case 13:
				if(thisSession.getTest8c() == null) {
					thisSession.setTest8c(temp1);
				} else {
					Log.v("DATA", "[TestSession] Test9c already exist.");
				}
				break;
			case 14:
				if(thisSession.getTest9() == null) {
					thisSession.setTest9(temp2);
				} else {
					Log.v("DATA", "[TestSession] Test10 already exist.");
				}
				break;
			}
		}
	}
	
	/*
	 * Writes the data from the provided session to an ArrayList of test datas, each element in the ArrayList contains an array of the various metrics
	 */
	public static ArrayList<String[]> writeTestData(TestSession session) {
		ArrayList<String[]> testSessionArrayList = new ArrayList<String[]>();
		Test1 temp1 = new Test1();
		Test2 temp2 = new Test2();
		Test3 temp3 = new Test3();
		Test4 temp4 = new Test4();
		Test5 temp5 = new Test5();
		for(int i = 1; i < 15; i++) {
			String[] arrayOfData = new String[NUMBER_OF_TEST_DATA+2]; //+2 because sessionID and testType is included
			arrayOfData[0] = session.getSessionID();
			arrayOfData[1] = Integer.toString(i);
			//determine the correct Test Type
			switch(i) {
			case 1:
				temp1 = session.getTest1o();
				break;
			case 2:
				temp1 = session.getTest1c();
				break;
			case 3:
				temp2 = session.getTest2();
				break;
			case 4:
				temp3 = session.getTest3();
				break;
			case 5:
				temp1 = session.getTest4o();
				break;
			case 6:
				temp1 = session.getTest4c();
				break;
			case 7:
				temp4 = session.getTest5();
				break;
			case 8:
				temp1 = session.getTest6o();
				break;
			case 9:
				temp1 = session.getTest6c();
				break;
			case 10:
				temp5 = session.getTest7o();
				break;
			case 11:
				temp5 = session.getTest7c();
				break;
			case 12:
				temp1 = session.getTest8o();
				break;
			case 13:
				temp1 = session.getTest8c();
				break;
			case 14:
				temp2 = session.getTest9();
				break;
			}
			//write the various tests into String array
			if(i == 1 || i == 2 || i == 5 || i == 6 || i == 8 || i == 9 || i == 12 || i == 13) {
				//Test1 Data
				arrayOfData[2] = Float.toString(temp1.getMeanX());
				arrayOfData[3] = Float.toString(temp1.getMeanY());
				arrayOfData[4] = Float.toString(temp1.getStdX());
				arrayOfData[5] = Float.toString(temp1.getStdY());
				arrayOfData[6] = Float.toString(temp1.getKurtosisX());
				arrayOfData[7] = Float.toString(temp1.getKurtosisY());
				arrayOfData[8] = Float.toString(temp1.getCovarianceXY());
				arrayOfData[9] = Float.toString(temp1.getStdVX());
				arrayOfData[10] = Float.toString(temp1.getStdVY());
				arrayOfData[11] = Float.toString(temp1.getKurtosisVX());
				arrayOfData[12] = Float.toString(temp1.getKurtosisVY());
				arrayOfData[13] = Float.toString(temp1.getCovarianceVXY());
				arrayOfData[14] = Float.toString(temp1.getRmsAX());
				arrayOfData[15] = Float.toString(temp1.getRmsAY());
				arrayOfData[16] = Float.toString(temp1.getStdAX());
				arrayOfData[17] = Float.toString(temp1.getStdAY());
				arrayOfData[18] = Float.toString(temp1.getKurtosisAX());
				arrayOfData[19] = Float.toString(temp1.getKurtosisAY());
				arrayOfData[20] = Float.toString(temp1.getCovarianceAXY());
				arrayOfData[21] = Float.toString(temp1.getRmsAX());
				arrayOfData[22] = Float.toString(temp1.getRmsAY());
			} else if(i == 10 || i == 11) {
				//Test5 Data
				arrayOfData[27] = Float.toString(temp5.getMeanPowerFreq());
				arrayOfData[28] = Float.toString(temp5.getPeakPowerFreq());
				arrayOfData[29] = Float.toString(temp5.getHighEnergy());
			} else if(i == 3 || i == 14) {
				//Test2 Data
				arrayOfData[2] = Float.toString(temp2.getMeanX());
				arrayOfData[3] = Float.toString(temp2.getMeanY());
				arrayOfData[4] = Float.toString(temp2.getStdX());
				arrayOfData[5] = Float.toString(temp2.getStdY());
				arrayOfData[6] = Float.toString(temp2.getKurtosisX());
				arrayOfData[7] = Float.toString(temp2.getKurtosisY());
				arrayOfData[8] = Float.toString(temp2.getCovarianceXY());
				arrayOfData[9] = Float.toString(temp2.getStdVX());
				arrayOfData[10] = Float.toString(temp2.getStdVY());
				arrayOfData[11] = Float.toString(temp2.getKurtosisVX());
				arrayOfData[12] = Float.toString(temp2.getKurtosisVY());
				arrayOfData[13] = Float.toString(temp2.getCovarianceVXY());
				arrayOfData[14] = Float.toString(temp2.getRmsAX());
				arrayOfData[15] = Float.toString(temp2.getRmsAY());
				arrayOfData[16] = Float.toString(temp2.getStdAX());
				arrayOfData[17] = Float.toString(temp2.getStdAY());
				arrayOfData[18] = Float.toString(temp2.getKurtosisAX());
				arrayOfData[19] = Float.toString(temp2.getKurtosisAY());
				arrayOfData[20] = Float.toString(temp2.getCovarianceAXY());
				arrayOfData[21] = Float.toString(temp2.getRmsAX());
				arrayOfData[22] = Float.toString(temp2.getRmsAY());
			} else if(i == 4) {
				//Test3 Data
				arrayOfData[2] = Float.toString(temp3.getMeanX());
				arrayOfData[3] = Float.toString(temp3.getMeanY());
				arrayOfData[4] = Float.toString(temp3.getStdX());
				arrayOfData[5] = Float.toString(temp3.getStdY());
				arrayOfData[6] = Float.toString(temp3.getKurtosisX());
				arrayOfData[7] = Float.toString(temp3.getKurtosisY());
				arrayOfData[8] = Float.toString(temp3.getCovarianceXY());
				arrayOfData[9] = Float.toString(temp3.getStdVX());
				arrayOfData[10] = Float.toString(temp3.getStdVY());
				arrayOfData[11] = Float.toString(temp3.getKurtosisVX());
				arrayOfData[12] = Float.toString(temp3.getKurtosisVY());
				arrayOfData[13] = Float.toString(temp3.getCovarianceVXY());
				arrayOfData[14] = Float.toString(temp3.getRmsAX());
				arrayOfData[15] = Float.toString(temp3.getRmsAY());
				arrayOfData[16] = Float.toString(temp3.getStdAX());
				arrayOfData[17] = Float.toString(temp3.getStdAY());
				arrayOfData[18] = Float.toString(temp3.getKurtosisAX());
				arrayOfData[19] = Float.toString(temp3.getKurtosisAY());
				arrayOfData[20] = Float.toString(temp3.getCovarianceAXY());
				arrayOfData[21] = Float.toString(temp3.getRmsAX());
				arrayOfData[22] = Float.toString(temp3.getRmsAY());
			} else if(i == 7) {
				//Test4 Data
				arrayOfData[23] = Float.toString(temp4.getMinX());
				arrayOfData[24] = Float.toString(temp4.getMaxX());
				arrayOfData[25] = Float.toString(temp4.getMinY());
				arrayOfData[26] = Float.toString(temp4.getMaxAccel());
			}
			testSessionArrayList.add(arrayOfData);
		}
		if(testSessionArrayList.size() != 14) {
			Log.v("DATA", "[TestSession] ArrayList is incomplete, size: " + testSessionArrayList.size());
		}
//		for(String[] sArray : testSessionArrayList) {
//			Log.v("PARSER", "[TestSession] array: " + Arrays.toString(sArray));
//		}
		return testSessionArrayList;
	}

	public String getProfileID() {
		return profileID;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public ArrayList<String> getTestIDs() {
		return testIDs;
	}

	public void setTestIDs(ArrayList<String> testIDs) {
		this.testIDs = testIDs;
	}

	public Test1 getTest1o() {
		return test1o;
	}

	public void setTest1o(Test1 test1o) {
		this.test1o = test1o;
	}

	public Test1 getTest1c() {
		return test1c;
	}

	public void setTest1c(Test1 test1c) {
		this.test1c = test1c;
	}

	public Test2 getTest2() {
		return test2;
	}

	public void setTest2(Test2 test2) {
		this.test2 = test2;
	}

	public Test3 getTest3() {
		return test3;
	}

	public void setTest3(Test3 test3) {
		this.test3 = test3;
	}

	public Test1 getTest4o() {
		return test4o;
	}

	public void setTest4o(Test1 test4o) {
		this.test4o = test4o;
	}

	public Test1 getTest4c() {
		return test4c;
	}

	public void setTest4c(Test1 test4c) {
		this.test4c = test4c;
	}

	public Test4 getTest5() {
		return test5;
	}

	public void setTest5(Test4 test5) {
		this.test5 = test5;
	}

	public Test1 getTest6o() {
		return test6o;
	}

	public void setTest6o(Test1 test6o) {
		this.test6o = test6o;
	}

	public Test1 getTest6c() {
		return test6c;
	}

	public void setTest6c(Test1 test6c) {
		this.test6c = test6c;
	}

	public Test5 getTest7o() {
		return test7o;
	}

	public void setTest7o(Test5 test7) {
		this.test7o = test7;
	}

	public Test5 getTest7c() {
		return test7c;
	}

	public void setTest7c(Test5 test8) {
		this.test7c = test8;
	}

	public Test1 getTest8o() {
		return test8o;
	}

	public void setTest8o(Test1 test8o) {
		this.test8o = test8o;
	}

	public Test1 getTest8c() {
		return test8c;
	}

	public void setTest8c(Test1 test8c) {
		this.test8c = test8c;
	}

	public Test2 getTest9() {
		return test9;
	}

	public void setTest9(Test2 test9) {
		this.test9 = test9;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public static TestSession getCurrentSession() {
		return currentSession;
	}

	public static void setCurrentSession(TestSession currentSession) {
		TestSession.currentSession = currentSession;
	}

	public static ArrayList<ArrayList<Float>> getArrayOfDemoGaitValues() {
		return arrayOfDemoGaitValues;
	}

	public static void setArrayOfDemoGaitValues(
			ArrayList<ArrayList<Float>> arrayOfDemoGaitValues) {
		TestSession.arrayOfDemoGaitValues = arrayOfDemoGaitValues;
	}
}
