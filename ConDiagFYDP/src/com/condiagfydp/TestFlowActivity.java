package com.condiagfydp;

import java.io.File;

import com.condiagfydp.database.AsyncTaskManager;
import com.condiagfydp.database.CreateTestSessionTask;
import com.condiagfydp.database.OnTaskCompleteListener;
import com.condiagfydp.database.TaskBase;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TestFlowActivity extends Activity implements ActionBar.OnNavigationListener, OnTaskCompleteListener{
	private AsyncTaskManager taskManager;

	TextView testTitle;
	TextView testDescription;
	Button continueButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_flow);
		// Set up the action bar to show a dropdown list.
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		// Show the Up button in the action bar.
		actionBar.setDisplayHomeAsUpEnabled(false);

		// Set up the dropdown list navigation in the action bar.
		actionBar.setListNavigationCallbacks(
		// Specify a SpinnerAdapter to populate the dropdown list.
				new ArrayAdapter<String>(actionBar.getThemedContext(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, new String[] {
								getString(R.string.title_TestFlow),
								getString(R.string.title_PatientProfile),
								getString(R.string.title_PatientSelection),
								getString(R.string.title_MainMenu), }), this);
		// Show the Up button in the action bar.
		//setupActionBar();
		
		//display the appropriate title
		testTitle = (TextView) findViewById(R.id.testTitle);
		testTitle.setText(TestProcedure.getTestPhaseTitle());
				
		//display the appropriate description
		testDescription = (TextView) findViewById(R.id.testDescription);
		testDescription.setText(TestProcedure.getTestPhaseDescription());
		
		//write the command file if needed
		TestProcedure.writeTestPhaseCommand();
		
		taskManager = new AsyncTaskManager(this, this);
	}
	@Override
	public boolean onNavigationItemSelected(int position, long id) {
//		Log.v("NAV", "navigation item: " + position);
		Intent intent = new Intent();
		switch (position) {
			case 0:
				//Do Nothing
				break;
			case 1: 
				intent = new Intent(this, PatientProfileActivity.class);
				startActivity(intent);
				break;
			case 2:
				intent = new Intent(this, PatientSelectionActivity.class);
				startActivity(intent);
				break;
			case 3:
				intent = new Intent(this, MainMenuActivity.class);
				startActivity(intent);
				break;
			default:
				//do nothing
				break;
		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test_flow, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			//DO NOTHING
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void continueFlow(View view) {
		//check if data has been received when needed
		boolean canNavigateToNext = true;
		if(TestProcedure.checkForData()) {
			String patientName = PatientProfile.getName();
			int patientSession = 0;
			File dataFile = new File(new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_COMMAND_FOLDERNAME), TestCommand.TEST_DATA_FILENAME + TestProcedure.getTestNumber() + TestCommand.TEST_DATA_FILETYPE);
			Log.v("FILE", "Date File length: " + dataFile.length());
			if(dataFile.exists()) {
				Log.v("FILE", "The Data file exists.");
				File savedDataFile;
				do {
					patientSession++;
					savedDataFile = new File(new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_DATA_FOLDERNAME), 
						TestCommand.SAVED_DATA_FILENAME + TestProcedure.getTestNumber() + patientName + "_" + patientSession + TestCommand.SAVED_DATA_FILETYPE);
				} while(savedDataFile.exists());
				
				//only copy the data file if it is not a demo
				if(!TestProcedure.isDemo){
					TestCommand.copy(dataFile, savedDataFile);
				}
				
				//Read and parse the datafile
				TestProcedure.readDataIntoStructure(TestSession.getCurrentSession(), TestProcedure.readDataFromFile(dataFile));
				
				//Delete the Datafile
				TestCommand.deleteCommand(dataFile);
				Log.v("TESTPROCEDURE", "[TestFlowActivity] Test Data: " + TestProcedure.getTestNumber() + " saved under /TestData");
			} else {
				canNavigateToNext = false;
				Log.v("FILE", "File: " + dataFile.getName() + " does not exist.");
				Toast.makeText(this, "Test Data has not been received yet.", Toast.LENGTH_SHORT).show();
			}
		}
		
		if(canNavigateToNext) {
			if(TestProcedure.isLastPhase()) {
				//query DB to create new session entry
				try {
		    		Log.v("DATABASE", "[ResultsActivity] Sending profileID: " + PatientProfile.getPatientID() + " and Date: " + TestSession.getCurrentSession().getDate());
		    		taskManager.setupTask(new CreateTestSessionTask(getResources(), PatientProfile.getPatientID(), TestSession.getCurrentSession().getDate()), true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (TestProcedure.isDemo && TestProcedure.getTestPhase() == 3) {
				//demo code should navigate to gait initiation plot result
				Intent intent = new Intent(this, GaitDemoPlotActivity.class);
				startActivity(intent);
			} else {
				TestProcedure.nextPhase();
				Intent intent = new Intent(this, TestFlowActivity.class);
				startActivity(intent);
			}
		}
	}

	@Override
	public void onTaskComplete(TaskBase<?, ?> task) {
		if(task.getClass() == CreateTestSessionTask.class){
			Object o = null;
			try {
				o = task.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.v("DATABASE", "[ResultsActivity] Received: " + (String) task.mResult);
			if (o != null){
				String result = (String) task.mResult;
				String sessionID = result.split(" ")[0];
				TestSession.getCurrentSession().setSessionID(sessionID);
				Intent intent = new Intent(this, ResultsActivity.class);
				Bundle b = new Bundle();
				b.putInt("listCounter", 0);
				intent.putExtras(b);
			    finish();
			    startActivity(intent);
			}
			else {
			}
		}
	}
}
