package com.condiagfydp;

import java.util.ArrayList;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.condiagfydp.database.AsyncTaskManager;
import com.condiagfydp.database.LoadTestSessionsTask;
import com.condiagfydp.database.OnTaskCompleteListener;
import com.condiagfydp.database.TaskBase;

public class PatientProfileActivity extends FragmentActivity implements
		ActionBar.OnNavigationListener, OnTaskCompleteListener {

	TextView patientDescription;
	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * current dropdown position.
	 */
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	private AsyncTaskManager taskManager;
	//private ArrayList<TestSession> listOfTestSessions = new ArrayList<TestSession>();
	private LinearLayout ll;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_profile);

		// Set up the action bar to show a dropdown list.
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		// Show the Up button in the action bar.
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Set up the dropdown list navigation in the action bar.
		actionBar.setListNavigationCallbacks(
		// Specify a SpinnerAdapter to populate the dropdown list.
				new ArrayAdapter<String>(actionBar.getThemedContext(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, new String[] {
								getString(R.string.title_PatientProfile),
								getString(R.string.title_PatientSelection),
								getString(R.string.title_MainMenu), }), this);
		
		taskManager = new AsyncTaskManager(this, this);
		
		//display Patient Profile
		patientDescription = (TextView)findViewById(R.id.patientDescription);
		if(PatientProfile.isMetric()) {
			patientDescription.setText("Name: " + PatientProfile.getName() + "\nAge: " + PatientProfile.getAge() + "\nHeight: "
					+ PatientProfile.getHeight() + "\nWeight: " + PatientProfile.getWeight() + "\nGender: " + PatientProfile.getGender()
					+ "\nSport: " + PatientProfile.getSport());
		} else {
			patientDescription.setText("Name: " + PatientProfile.getName() + "\nAge: " + PatientProfile.getAge() + "\nHeight: "
					+ PatientProfile.getIHeight() + "(" + PatientProfile.getHeight() + ")" + "\nWeight: " + PatientProfile.getIWeight() 
					+ "(" + PatientProfile.getWeight() + ")" + "\nGender: " + PatientProfile.getGender() + "\nSport: " + PatientProfile.getSport());
		}
		
		//query DB for patient test history, only if profile not loaded.
		if(PatientProfile.getListOfTestSessions() == null) {
			try {
	    		Log.v("DATABASE", "Sending Patient ID: " + PatientProfile.getPatientID());
	    		taskManager.setupTask(new LoadTestSessionsTask(getResources(), PatientProfile.getPatientID()), true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			populateTestSessions();
		}
		
		//this linear layout displays the list of test histories
		ll = (LinearLayout) findViewById(R.id.testHistory);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current dropdown position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current dropdown position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(int position, long id) {
//		Log.v("NAV", "navigation item: " + position);
		Intent intent = new Intent();
		switch (position) {
			case 0:
				//Do Nothing
				break;
			case 1:
				intent = new Intent(this, PatientSelectionActivity.class);
				startActivity(intent);
				break;
			case 2:
				intent = new Intent(this, MainMenuActivity.class);
				startActivity(intent);
				break;
			default:
				//do nothing
				break;
		}
		return true;
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_patient_profile_dummy, container, false);
			TextView dummyTextView = (TextView) rootView
					.findViewById(R.id.section_label);
			dummyTextView.setText(Integer.toString(getArguments().getInt(
					ARG_SECTION_NUMBER)));
			return rootView;
		}
	}
	
	public void startTestBeta(View view) {
		if(TestCommand.getListOfCommands() == null) {
			ArrayList<String> listOfCommands = new ArrayList<String>();
			listOfCommands.add("10");
			listOfCommands.add("01");
			listOfCommands.add("02");
			listOfCommands.add("03");
			listOfCommands.add("04");
			listOfCommands.add("05");
			listOfCommands.add("06");
			listOfCommands.add("07");
			listOfCommands.add("08");
			Log.v("TESTPROCEDURE", "[PatientProfileActivity] The list of commands are the following: " + listOfCommands.toString());
			TestCommand.setListOfCommands(listOfCommands);
		}
		Intent intent = new Intent(this, BetaTestStartActivity.class);
		startActivity(intent);
	}
	
	public void startTest(View view) {
		TestProcedure.resetTest();
		TestSession.setCurrentSession(new TestSession());
		TestSession.getCurrentSession().setProfileID(PatientProfile.getPatientID());
		Intent intent = new Intent(this, TestFlowActivity.class);
		startActivity(intent);
	}
	
	public void openTestDetails(View view) {
		Intent intent = new Intent(this, TestSessionDetails.class);
		startActivity(intent);
	}

	/*
	 * This handles the behaviour after the results from the DB query is received
	 * @see com.condiagfydp.database.OnTaskCompleteListener#onTaskComplete(com.condiagfydp.database.TaskBase)
	 */
	@Override
	public void onTaskComplete(TaskBase<?, ?> task) {
		if(task.getClass() == LoadTestSessionsTask.class){
			Object o = null;
			try {
				o = task.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.v("DATABASE", "[PatientProfileActivity] Received: " + (String) task.mResult);
			if (o != null){
				String result = (String) task.mResult;
				String[] resultArray = result.split(" ");
				//determine how many session
				int numOfTestSessions = resultArray.length/17;
				for(int i = 0; i < numOfTestSessions; i++) {
					ArrayList<String> arrayOfTestIDs = new ArrayList<String>();
					//for each session, store the testIDs
					for(int j = i*17, k = 0; k < 14; j++, k++) {
						arrayOfTestIDs.add(resultArray[j+3]);
					}
					String profileID = resultArray[(i*17)];
					String sessionID = resultArray[(i*17)+1];
					String date = resultArray[(i*17)+2];
					TestSession temp = new TestSession(profileID, sessionID, arrayOfTestIDs, date);
					Log.v("DATA", "[PatientProfileActivity] Session: " + temp.getSessionID() + "Test IDs: " + temp.getTestIDs());
					listOfTestSessions.add(temp);
				}
				Log.v("DATA", "[PatientProfileActivity] There are " + listOfTestSessions.size() + " historical test data for patient " + PatientProfile.getName());
				PatientProfile.setListOfTestSessions(listOfTestSessions);
				//populate the list with the list of sessions obtained from DB
				populateTestSessions();
//				for(TestSession session : listOfTestSessions) {
//					Button testHistoryButton = new Button(this);
//					testHistoryButton.setText(session.getDate());
//					ll.addView(testHistoryButton);
//					final String testSessionID = session.getSessionID();
//					testHistoryButton.setOnClickListener(new OnClickListener() {
//						@Override
//						public void onClick(View view) {
////							Bundle b = new Bundle();
////							b.putString("listCounter", testSessionID);
////							Intent intent = getIntent();
////							intent.putExtras(b);
////						    finish();
//							TestSession.setCurrentSession(TestSession.findSessionByID(testSessionID));
//							openTestDetails(view);
//						}
//					});
//				}
				
			}
			else {
			}
		}
	}
	
	public void populateTestSessions() {
		for(TestSession session : PatientProfile.getListOfTestSessions()) {
			Button testHistoryButton = new Button(this);
			testHistoryButton.setText(session.getDate());
			ll.addView(testHistoryButton);
			final String testSessionID = session.getSessionID();
			testHistoryButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
//					Bundle b = new Bundle();
//					b.putString("listCounter", testSessionID);
//					Intent intent = getIntent();
//					intent.putExtras(b);
//				    finish();
					TestSession.setCurrentSession(TestSession.findSessionByID(testSessionID));
					openTestDetails(view);
				}
			});
		}
	}
}
