package com.condiagfydp;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainMenuActivity extends Activity {

	SharedPreferences sharedPrefs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		//Clear any lingering command files left over from previous sessions.
		//TODO remove this section ones app flow can handle command deletion
		File file = new File(new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_COMMAND_FOLDERNAME), TestCommand.TEST_COMMAND_FILENAME);
		if(!file.exists()) {
			Log.v("FILE", "There are no test commands to delete!");
		} else {
			TestCommand.deleteCommand(file);
		}
		//create required directories
		File testCommandFolder = new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_COMMAND_FOLDERNAME);
		File testDataFolder = new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_DATA_FOLDERNAME);
		File patientListFolder = new File(Environment.getExternalStorageDirectory(), PatientList.PATIENT_LIST_FOLDERNAME);
        if (!testCommandFolder.exists()) {
        	Log.v("FILE", "[MainMenuActivity] Creating TestCommand directory.");
        	testCommandFolder.mkdirs();
        }
        if (!testDataFolder.exists()) {
        	Log.v("FILE", "[MainMenuActivity] Creating TestData directory.");
        	testDataFolder.mkdirs();
        	InputStream concussedgait = getResources().openRawResource(R.raw.concussedgait);
        	File concussedGaitFile = new File(testDataFolder, TestCommand.CONCUSSED_DATA_FILENAME + TestCommand.TEST_DATA_FILETYPE);
        	installFile(concussedgait, concussedGaitFile);
        }
        if (!patientListFolder.exists()) {
        	Log.v("FILE", "[MainMenuActivity] Creating PatientList directory.");
        	patientListFolder.mkdirs();
        	InputStream existingList = getResources().openRawResource(R.raw.patientlist);
        	File patientListFile = new File(patientListFolder, PatientList.PATIENT_LIST_FILENAME);
        	installFile(existingList, patientListFile);
        }

        TestProcedure.isDemo = sharedPrefs.getBoolean("demo_checkbox", false);
	}
	
	public void installFile(InputStream inputStream, File targetFile) {
		OutputStream outputStream = null;
    	try {
			outputStream = new FileOutputStream(targetFile);
		} catch (FileNotFoundException e1) {
			Log.v("FILE", "[MainMenuActivity] Cannot find target file.");
		}
    	int read = 0;
		byte[] bytes = new byte[1024];
 
		try {
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			Log.v("FILE", "[MainMenuActivity] IO Exception reading file.");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
 
        case R.id.action_settings:
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, 1);
            break;
 
        }
 
        return true;
    }
	
	public void addNewPatient(View view) {
		TestProcedure.isDemo = sharedPrefs.getBoolean("demo_checkbox", false);
		Intent intent = new Intent(this, ProfileCreationActivity.class);
		startActivity(intent);
	}

	public void selectExistingPatient(View view) {
		TestProcedure.isDemo = sharedPrefs.getBoolean("demo_checkbox", false);
		if(PatientList.readPatientList() == -1) {
			Toast.makeText(getApplicationContext(), "There are no existing patient profiles. Please create a new patient profile.", Toast.LENGTH_LONG).show();
		} else {
			Intent intent = new Intent(this, PatientSelectionActivity.class);
	    	startActivity(intent);
		}
	}
	
	public void shutdown(View view) {
		TestCommand newCommand = new TestCommand("00");
		newCommand.createFile();
//		File testCommand = newCommand.createFile();
		File dataFile = new File(new File(Environment.getExternalStorageDirectory(), TestCommand.TEST_COMMAND_FOLDERNAME), TestCommand.TEST_DATA_FILENAME);
		Log.v("FILE", "Date File length: " + dataFile.length());
		if(dataFile.exists()) {
			Log.v("FILE", "The Data file exists and will be deleted before writing next command.");
			TestCommand.deleteCommand(dataFile);
		}
		Toast.makeText(this, "Command file has been saved as: " + TestCommand.TEST_COMMAND_FILENAME, Toast.LENGTH_SHORT).show();
	}
}
