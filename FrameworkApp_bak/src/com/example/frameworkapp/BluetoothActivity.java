package com.example.frameworkapp;

import java.util.ArrayList;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class BluetoothActivity extends Activity {

	private CheckBox checkBox1;
	private CheckBox checkBox2;
	private CheckBox checkBox3;
	private CheckBox checkBox4;
	private ArrayList<CheckBox> listOfCheckboxes = new ArrayList<CheckBox>();
	
	// Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    // Layout Views
    private ListView mConversationView;
    private EditText mOutEditText;
    private Button mScanButton;
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private ArrayAdapter<String> mConversationArrayAdapter;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private BluetoothCommunicationService mCommService = null;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bluetooth);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		listOfCheckboxes.add((CheckBox) findViewById(R.id.checkBox1));
    	listOfCheckboxes.add((CheckBox) findViewById(R.id.checkBox2));
    	listOfCheckboxes.add((CheckBox) findViewById(R.id.checkBox3));
    	listOfCheckboxes.add((CheckBox) findViewById(R.id.checkBox4));
    	if(mBluetoothAdapter == null) {
    		//Device does not support Bluetooth
    		Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
    	} else {
    		if(!mBluetoothAdapter.isEnabled()){
    			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
    			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    		}
    		Log.v("BLUETOOTH", "Device Supports Bluetooth");
    		populateBluetoothList();
    	}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bluetooth, menu);
		return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

	public void populateBluetoothList(){
    	Log.v("BLUETOOTH", "Populating Bluetooth List");
    	Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
    	Log.v("BLUETOOTH", "Number Of Paired Devices: " + pairedDevices.size());
    	//If there are paired devices
    	if(pairedDevices.size() > 0) {
    		//Loop through paired devices
    		int i = 0;
    		for(BluetoothDevice device : pairedDevices) {
    			//Add the name and address to an array adapter to show in a ListView
    			String s = "Name: " + device.getName() + " Address: " + device.getAddress();
    			Log.v("BLUETOOTH", s);
    			listOfCheckboxes.get(i).setText(s);
    			i++;
    		}
    	}
	}
	
	/*
	 * Runs this method when returning to this activity from another activity
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		//If bluetooth is still disabled, continue to prompt until enable bluetooth
		Log.v("BLUETOOTH", "RequestCode: " + requestCode + " ResultCode: " + resultCode);
		if(resultCode != RESULT_OK){
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		} else {
			Log.v("BLUETOOTH", "Device Supports Bluetooth");
			populateBluetoothList();
		}
	}

	public void scanBluetooth(View view){
		// Create a BroadcastReceiver for ACTION_FOUND
		final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		    public void onReceive(Context context, Intent intent) {
		    	Log.v("Bluetooth", "Starting Bluetooth Scan...");
		        String action = intent.getAction();
		        // When discovery finds a device
		        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
		            // Get the BluetoothDevice object from the Intent
		            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		            // Add the name and address to an array adapter to show in a ListView
//		            mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
		            String s = "Name: " + device.getName() + " Address: " + device.getAddress();
	    			Log.v("BLUETOOTH", s);
		            listOfCheckboxes.get(0).setText(s);
		        }
		    }
		};
		// Register the BroadcastReceiver
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
		Log.v("BLUETOOTH", "Bluetooth Scan Complete.");
	}
}
