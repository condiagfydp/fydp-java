package com.example.frameworkapp;

import android.content.res.Resources;
import android.util.Log;

public final class AuthenticationTask extends TaskBase<Void, String> {

	private String text;
	private String text2;
	private String text3;
	private final String message = "Sending Data...";
	
	/* UI Thread */
	public AuthenticationTask(Resources resources, String text, String text2, String text3) {
		super(resources);
		this.text = text;
		this.text2 = text2;
		this.text3 = text3;
		mProgressMessage = message;
	}    

	/* Separate Thread */
	@Override
	protected String doInBackground(Void... args) {
		String result = null;
		if (isCancelled()) {
			result = "cancelled";
		}
		try {
			String s = null;
			if(this.text3 == null){
				s = Networks.HOST + "?message=" + text + "&message2=" + text2;
			} else if (this.text2 == null) {
				s = Networks.HOST + "?message=" + text;
			} else if (this.text == null) {
				Log.v("DATABASE", "All messages are null");
			} else {
				s = Networks.HOST + "?message=" + text + "&message2=" + text2 + "&message3=" + text3;
			}
			result = Networks.httpClient(s);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}
}