package com.example.frameworkapp;

import android.content.res.Resources;

public final class QueryTask extends TaskBase<Void, String> {

	private final String message = "Getting Data...";
	
	/* UI Thread */
	public QueryTask(Resources resources) {
		super(resources);
		mProgressMessage = message;
	}    

	/* Separate Thread */
	@Override
	protected String doInBackground(Void... args) {
		String result = null;
		if (isCancelled()) {
			result = "cancelled";
		}
		try {
			result = Networks.httpClient(Networks.HOST);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}
}