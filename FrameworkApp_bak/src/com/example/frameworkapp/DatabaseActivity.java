package com.example.frameworkapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class DatabaseActivity extends Activity implements OnTaskCompleteListener{
	
	private AsyncTaskManager taskManager;
	private EditText editText;
	private EditText editText2;
	private EditText editText3;
	private EditText editText4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_database);
		editText = (EditText) findViewById(R.id.edit_message);
    	editText2 = (EditText) findViewById(R.id.edit_message_2);
    	editText3 = (EditText) findViewById(R.id.edit_message_3);
    	editText4 = (EditText) findViewById(R.id.edit_message_4);
    	taskManager = new AsyncTaskManager(this, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.database, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	/** Called when the user clicks the Send button */
    public void sendMessage(View view) {
        // Do something in response to button
    	String message2 = editText2.getText().toString();
    	String message3 = editText3.getText().toString();
    	String message4 = editText4.getText().toString();
    	try {
    		Log.v("DATABASE", "Sending: " + message2 + " " + message3 + " " + message4);
    		taskManager.setupTask(new AuthenticationTask(getResources(), message2, message3, message4), true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	editText.setText("");
    	editText2.setText("");
    	editText3.setText("");
    	editText4.setText("");
    }
    
    /*
     * Goes to the Bluetooth page
     */
    public void goToBluetooth(View view) {
    	Intent intent = new Intent(this, BluetoothActivity.class);
    	startActivity(intent);
    }
    
    /*
     * Goes to the Graph page
     */
    public void goToGraph(View view) {
    	Intent intent = new Intent(this, GraphActivity.class);
    	startActivity(intent);
    }
    
    /** Called when the user clicks the Get button */
    public void getMessage(View view) {
        // Do something in response to button
    	try {
    		taskManager.setupTask(new QueryTask(getResources()), true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_settings:
                //openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

	public void onTaskComplete(TaskBase<?,?> task) {
		if(task.getClass() == QueryTask.class){
			Object o = null;
			try {
				o = task.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.v("DATABASE", "Received: " + (String) task.mResult);
			if (o != null){
				String result = (String) task.mResult;
				editText.setText(result.split(" ")[0]);
				editText2.setText(result.split(" ")[1]);
				editText3.setText(result.split(" ")[2]);
				editText4.setText(result.split(" ")[3]);
			}
			else {
			}
		}
	}

}
