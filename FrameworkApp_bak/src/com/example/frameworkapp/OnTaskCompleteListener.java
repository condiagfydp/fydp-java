package com.example.frameworkapp;



public interface OnTaskCompleteListener {
    // Notifies about task completeness
    void onTaskComplete(TaskBase<?,?> task);
}