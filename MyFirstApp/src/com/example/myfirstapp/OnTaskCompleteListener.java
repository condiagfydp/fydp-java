package com.example.myfirstapp;



public interface OnTaskCompleteListener {
    // Notifies about task completeness
    void onTaskComplete(TaskBase<?,?> task);
}